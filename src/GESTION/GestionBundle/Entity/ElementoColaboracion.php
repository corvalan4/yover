<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\ElementoColaboracionRepository")
 * @ORM\Table(name="elementocolaboracion")
 */
class ElementoColaboracion{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Colaboracion", inversedBy="elementoscolaboracion")
     * @ORM\JoinColumn(name="colaboracion_id", referencedColumnName="id")
     */
    protected $colaboracion;

    /**
     * @ORM\ManyToOne(targetEntity="ElementoStock", inversedBy="elementoscolaboracion")
     * @ORM\JoinColumn(name="elementostock_id", referencedColumnName="id")
     */
    protected $elementostock;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precio;
	
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $cod_estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return "";
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cod_estado
     *
     * @param string $codEstado
     * @return Elemento
     */
    public function setCodEstado($codEstado)
    {
        $this->cod_estado = $codEstado;
    
        return $this;
    }

    /**
     * Get cod_estado
     *
     * @return string 
     */
    public function getCodEstado()
    {
        return $this->cod_estado;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return ElementoColaboracion
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    
        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set colaboracion
     *
     * @param \GESTION\GestionBundle\Entity\Colaboracion $colaboracion
     * @return ElementoColaboracion
     */
    public function setColaboracion(\GESTION\GestionBundle\Entity\Colaboracion $colaboracion = null)
    {
        $this->colaboracion = $colaboracion;
    
        return $this;
    }

    /**
     * Get colaboracion
     *
     * @return \GESTION\GestionBundle\Entity\Colaboracion 
     */
    public function getColaboracion()
    {
        return $this->colaboracion;
    }

    /**
     * Set elemento
     *
     * @param \GESTION\GestionBundle\Entity\ElementoStock $elemento
     * @return ElementoColaboracion
     */
    public function setElementostock(\GESTION\GestionBundle\Entity\ElementoStock $elemento = null)
    {
        $this->elementostock = $elemento;
    
        return $this;
    }

    /**
     * Get elemento
     *
     * @return \GESTION\GestionBundle\Entity\ElementoStock 
     */
    public function getElementostock()
    {
        return $this->elementostock;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return ElementoColaboracion
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    
        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio()
    {
        return $this->precio;
    }
}