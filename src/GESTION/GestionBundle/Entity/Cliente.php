<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\ClienteRepository")
 * @ORM\Table(name="cliente")
 */
class Cliente{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=400)
     */
    protected $razonSocial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $localidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $direccion;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $mail;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $clave;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento = 0;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $tipo='CLIENTE';

	/**
	* @ORM\OneToMany(targetEntity="Colaboracion", mappedBy="cliente")
	*/
	protected $colaboraciones;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';


    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->colaboraciones = new ArrayCollection();
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
			return $this->getRazonSocial();
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return Cliente
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;
    
        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Cliente
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Cliente
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    
        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Cliente
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    
        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Cliente
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Cliente
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add colaboraciones
     *
     * @param \GESTION\GestionBundle\Entity\Colaboracion $colaboraciones
     * @return Cliente
     */
    public function addColaboracione(\GESTION\GestionBundle\Entity\Colaboracion $colaboraciones)
    {
        $this->colaboraciones[] = $colaboraciones;
    
        return $this;
    }

    /**
     * Remove colaboraciones
     *
     * @param \GESTION\GestionBundle\Entity\Colaboracion $colaboraciones
     */
    public function removeColaboracione(\GESTION\GestionBundle\Entity\Colaboracion $colaboraciones)
    {
        $this->colaboraciones->removeElement($colaboraciones);
    }

    /**
     * Get colaboraciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getColaboraciones()
    {
        return $this->colaboraciones;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     * @return Cliente
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;
    
        return $this;
    }

    /**
     * Get cuit
     *
     * @return string 
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set localidad
     *
     * @param string $localidad
     * @return Cliente
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;
    
        return $this;
    }

    /**
     * Get localidad
     *
     * @return string 
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Cliente
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    
        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }
}