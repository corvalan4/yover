<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\AdjuntoRepository")
 * @ORM\Table(name="adjunto")
 */
class Adjunto{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    protected $contenido;
	
    /**
     * @ORM\ManyToOne(targetEntity="Colaboracion", inversedBy="adjuntos")
     * @ORM\JoinColumn(name="colaboracion_id", referencedColumnName="id")
     */
    protected $colaboracion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $cod_estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getNombre();
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Elemento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cod_estado
     *
     * @param string $codEstado
     * @return Elemento
     */
    public function setCodEstado($codEstado)
    {
        $this->cod_estado = $codEstado;
    
        return $this;
    }

    /**
     * Get cod_estado
     *
     * @return string 
     */
    public function getCodEstado()
    {
        return $this->cod_estado;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return Adjunto
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;
    
        return $this;
    }

    /**
     * Get contenido
     *
     * @return string 
     */
    public function getContenido()
    {
        return $this->contenido;
    }
	
    public function getContenidoBlob()
    {
        return stream_get_contents( $this->contenido );
    }


    /**
     * Set colaboracion
     *
     * @param \GESTION\GestionBundle\Entity\Colaboracion $colaboracion
     * @return Adjunto
     */
    public function setColaboracion(\GESTION\GestionBundle\Entity\Colaboracion $colaboracion = null)
    {
        $this->colaboracion = $colaboracion;
    
        return $this;
    }

    /**
     * Get colaboracion
     *
     * @return \GESTION\GestionBundle\Entity\Colaboracion 
     */
    public function getColaboracion()
    {
        return $this->colaboracion;
    }
}