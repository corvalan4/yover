<?php

namespace GESTION\GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', 'text', array ('attr'=> array('pattern'=>'|^[a-zA-Z0-9]*$|', 'title'=>'Solo letras y numeros. Sin espacios ni caracteres especiales.')))
            ->add('clave')
            ->add('perfil', 'choice', array (
				'attr'=> array('class'=>'form-control'),
				'choices' => array(
                    'ADMINISTRADOR' => 'ADMINISTRADOR',
					'OPERADOR' => 'OPERADOR'
		   		)))
            ->add('mail')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GESTION\GestionBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'usuario';
    }
}
