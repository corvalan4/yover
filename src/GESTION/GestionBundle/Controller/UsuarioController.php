<?php

namespace GESTION\GestionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use GESTION\GestionBundle\Entity\Usuario;
use GESTION\GestionBundle\Form\UsuarioType;
use GESTION\GestionBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Usuario controller.
 *
 */
class UsuarioController extends Controller
{

	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionSvc;	
    /**
     * Lists all Usuario entities.
     *
     */
    public function indexAction()
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
			$entities = $em->getRepository('GESTIONGestionBundle:Usuario')->findBy(array('estado'=>'A'), array('login'=>'ASC'));

			return $this->render('GESTIONGestionBundle:Usuario:index.html.twig', array(
				'entities' => $entities,
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    public function usuariowebAction()
    {
		if(!$this->sessionSvc->isLogged()){
			return $this->redirect($this->generateUrl('_homepage'));
		}
		
		$em = $this->getDoctrine()->getManager();
		$entities = $em->getRepository('GESTIONGestionBundle:UsuarioWeb')->getAll();									

		return $this->render('GESTIONGestionBundle:UsuarioWeb:index.html.twig', array(
			'entities' => $entities,
		));
    }

    /**
     * Creates a new Usuario entity.
     *
     */
    public function createAction(Request $request)
    {
		if($this->sessionSvc->isLogged()){
			$entity = new Usuario();
			$form = $this->createCreateForm($entity);
			$form->handleRequest($request);
			$em = $this->getDoctrine()->getManager();
	
			if ($form->isValid()) {
				$em->persist($entity);
				$em->flush();

				$this->sessionSvc->addFlash('msgOk','Usuario creado exitosamente.');			
	
				return $this->redirect($this->generateUrl('usuario'));
			}
	
			return $this->render('GESTIONGestionBundle:Usuario:new.html.twig', array(
				'entity' => $entity,
				'form'   => $form->createView(),
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
     * Creates a form to create a Usuario entity.
     *
     * @param Usuario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Usuario $entity)
    {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('usuario_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn middle-first crear', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new Usuario entity.
     *
     */
    public function newAction()
    {
		if($this->sessionSvc->isLogged()){
			$entity = new Usuario();
			$form   = $this->createCreateForm($entity);
	
			return $this->render('GESTIONGestionBundle:Usuario:new.html.twig', array(
				'entity' => $entity,
				'form'   => $form->createView(),
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }
	
    /**
     * Finds and displays a Usuario entity.
     *
     */
    public function showAction($id)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
		
			$entity = $em->getRepository('GESTIONGestionBundle:Usuario')->find($id);
		
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Usuario entity.');
			}
		
			$deleteForm = $this->createDeleteForm($id);
		
			return $this->render('GESTIONGestionBundle:Usuario:show.html.twig', array(
				'entity'      => $entity,
				'delete_form' => $deleteForm->createView(),
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
     * Displays a form to edit an existing Usuario entity.
     *
     */
    public function editAction($id)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('GESTIONGestionBundle:Usuario')->find($id);
	
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Usuario entity.');
			}
	
			$editForm = $this->createEditForm($entity);
			$deleteForm = $this->createDeleteForm($id);
	
			return $this->render('GESTIONGestionBundle:Usuario:edit.html.twig', array(
				'entity'      => $entity,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
    * Creates a form to edit a Usuario entity.
    *
    * @param Usuario $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Usuario $entity)
    {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('usuario_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn middle-first')));

        return $form;
    }
    /**
     * Edits an existing Usuario entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
	        $em->flush();

			$this->sessionSvc->addFlash('msgOk','Usuario modificado exitosamente.');			

            return $this->redirect($this->generateUrl('usuario'));
        }

        return $this->render('GESTIONGestionBundle:Usuario:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Usuario entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GESTIONGestionBundle:Usuario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Usuario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('usuario'));
    }

    public function aprobarusuarioAction($id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:UsuarioWeb')->find($id);
		$entity->setEstado('A');
		$em->flush();
		$this->sessionSvc->addFlash('msgOk','Usuario aprobado.');			
		
        return $this->redirect($this->generateUrl('usuario_web'));
    }

    public function desaprobarusuarioAction($id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:UsuarioWeb')->find($id);
		$entity->setEstado('B');
		$em->flush();
		$this->sessionSvc->addFlash('msgOk','Usuario desaprobado.');			
		
        return $this->redirect($this->generateUrl('usuario_web'));
    }

    /**
     * Creates a form to delete a Usuario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar', 'attr'=> array('class'=>'btn')))
            ->getForm()
        ;
    }
}
