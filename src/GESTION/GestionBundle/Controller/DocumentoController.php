<?php

namespace GESTION\GestionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

use GESTION\GestionBundle\Entity\StockUnidad;
use GESTION\GestionBundle\Entity\Documento;
use GESTION\GestionBundle\Entity\Producto;
use GESTION\GestionBundle\Entity\ProductoDocumento;
use GESTION\GestionBundle\Entity\MovimientoCC;
use GESTION\GestionBundle\Form\DocumentoType;
use GESTION\GestionBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Ps\PdfBundle\Annotation\Pdf;

/**
 * Documento controller.
 *
 */
class DocumentoController extends Controller
{

	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionSvc;	

    /**
     * Lists all Documento entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GESTIONGestionBundle:Documento')->findAll();

        return $this->render('GESTIONGestionBundle:Documento:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function gastosAction(Request $request)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();

			if(empty($request->get('desde'))){
				$desde = new \DateTime('NOW -30 days');
			}else{
				$desde = new \DateTime($request->get('desde'));
			}
			
			if(empty($request->get('hasta'))){
				$hasta = new \DateTime('NOW +1 days');
			}else{
				$hasta = new \DateTime($request->get('hasta'));
			}
	
			$entities = $em->getRepository('GESTIONGestionBundle:Documento')->filtro($desde, $hasta);
			$tiposGasto = $em->getRepository('GESTIONGestionBundle:TipoGasto')->findBy(array('estado'=>'A'));
	
			return $this->render('GESTIONGestionBundle:Documento:gastos.html.twig', array(
				'entities' => $entities,
				'desde' => $desde->format('Y-m-d'),
				'hasta' => $hasta->format('Y-m-d'),
				'tiposGasto' => $tiposGasto,
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
     * Creates a new Documento entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Documento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

		$em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
			$fecha = new \DateTime($request->get('fecha'));
			$entity->setFecha($fecha);
			$em->persist($entity);
            $em->flush();
	
     	    if($entity->getTipoDocumento()=="G"){
				$this->sessionSvc->addFlash('msgOk','Gasto registrado.');
				return $this->redirect($this->generateUrl('documento_gastos'));
			}			
        }
		
		return $this->render('GESTIONGestionBundle:Documento:new.html.twig', array(
            'entity'   => $entity,
            'form'     => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Documento entity.
     *
     * @param Documento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Documento $entity)
    {
        $form = $this->createForm(new DocumentoType($entity->getTipoDocumento()), $entity, array(
            'action' => $this->generateUrl('documento_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr'=> array('class'=>'btn middle-first crear', 'onclick'=>'ocultar(this.id)')));

        return $form;
    }

    /**
     * Displays a form to create a new Documento entity.
     *
     */
    public function newAction(Request $request, $tipo)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
			$entity = new Documento();
			$entity->setTipoDocumento($tipo);
			$form   = $this->createCreateForm($entity);
									
			return $this->render('GESTIONGestionBundle:Documento:new.html.twig', array(
				'entity' => $entity,
				'form'   => $form->createView())
			);
		}else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
     * Finds and displays a Documento entity.
     *
     */
    public function showAction($id)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('GESTIONGestionBundle:Documento')->find($id);
	
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Documento entity.');
			}
			
			if($entity->getTipoDocumento()!='F'){
				return $this->render('GESTIONGestionBundle:Documento:show.html.twig', array(
					'entity'      => $entity,
				));
			}else{
				return $this->render('GESTIONGestionBundle:Documento:showFactProv.html.twig', array(
					'entity'      => $entity,
				));
			}			
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    public function imprimirAction($id)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('GESTIONGestionBundle:Documento')->find($id);
	
			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Documento entity.');
			}
	
			$pdfGenerator = $this->get('siphoc.pdf.generator');
			$pdfGenerator->setName('RECIBO CLIENTE '.$entity->getId().'.pdf');
			return $pdfGenerator->downloadFromView(
				'GESTIONGestionBundle:Documento:imprimir.html.twig', array(
					'entity' => $entity,)
				);
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
     * Displays a form to edit an existing Documento entity.
     *
     */
    public function editAction($id)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('GESTIONGestionBundle:Documento')->find($id);
		
			$editForm = $this->createEditForm($entity);

	
			return $this->render('GESTIONGestionBundle:Documento:edit.html.twig', array(
				'entity'      => $entity,
				'edit_form'   => $editForm->createView(),
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
    * Creates a form to edit a Documento entity.
    *
    * @param Documento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Documento $entity)
    {
        $form = $this->createForm(new DocumentoType($entity->getTipoDocumento()), $entity, array(
            'action' => $this->generateUrl('documento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Documento entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Documento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
			$fecha = new \DateTime($request->get('fecha'));
			$entity->setFecha($fecha);
			$em->flush();
			
			if($entity->getTipoDocumento()=="G"){
				$this->sessionSvc->addFlash('msgOk','Gasto modificado.');
				return $this->redirect($this->generateUrl('documento_gastos'));
			}			
        }

        return $this->render('GESTIONGestionBundle:Documento:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    public function borrarAction(Request $request, $id)
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
	
			$entity = $em->getRepository('GESTIONGestionBundle:Documento')->find($id);
			$stock = '';		
			$entity->setEstado('B');
			$entity->getMovimientocc()->setEstado('B');
			if($entity->getTipoDocumento()=='F'){
				foreach($entity->getProductosDocumento() as $prodDoc){
					$stock = $prodDoc->getProducto()->getStock() - $prodDoc->getCantidad();
					if($stock<0){$stock=0;}
					$prodDoc->getProducto()->setStock($stock);
				}		
			}
			$em->flush();
			
			if($entity->getTipoDocumento()=="F"){
				$this->sessionSvc->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_compras'));
			}
			if($entity->getTipoDocumento()=="R"){
				$this->sessionSvc->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_cobranzas'));
			}
			if($entity->getTipoDocumento()=="G"){
				$this->sessionSvc->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_gastos'));
			}
			if($entity->getTipoDocumento()=="RP"){
				$this->sessionSvc->addFlash('msgOk','Baja satisfactoria.');
				return $this->redirect($this->generateUrl('documento_pagos'));
			}
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }

    /**
     * Deletes a Documento entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GESTIONGestionBundle:Documento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Documento entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('documento'));
    }

    /**
     * Creates a form to delete a Documento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('documento_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar', 'attr'=> array('class'=>'btn')))
            ->getForm()
        ;
    }
	
    public function filtrogastoAction(Request $request)
    {
		if(!$this->sessionSvc->isLogged()){
			return $this->redirect($this->generateUrl('_homepage'));
		}
		
		$em = $this->getDoctrine()->getManager();

		if(empty($request->get('desde'))){
			$desde = new \DateTime('NOW -30 days');
		}else{
			$desde = new \DateTime($request->get('desde'));
		}
		
		if(empty($request->get('hasta'))){
			$hasta = new \DateTime('NOW +1 days');
		}else{
			$hasta = new \DateTime($request->get('hasta'));
		}

		$entities = $em->getRepository('GESTIONGestionBundle:Documento')->filtro($desde, $hasta, $request->get('tipoGasto'));
		$tiposGasto = $em->getRepository('GESTIONGestionBundle:TipoGasto')->findBy(array('estado'=>'A'));

		return $this->render('GESTIONGestionBundle:Documento:gastos.html.twig', array(
			'entities' => $entities,
			'desde' => $desde->format('Y-m-d'),
			'hasta' => $hasta->format('Y-m-d'),
			'tiposGasto' => $tiposGasto,
		));
	}
    
	public function informeVentasAction()
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
			return $this->render('GESTIONGestionBundle:Documento:filtro.html.twig', array(
				'tipo'=>'ventas',
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }
    public function informeGastosAction()
    {
		if($this->sessionSvc->isLogged()){
			$em = $this->getDoctrine()->getManager();
			return $this->render('GESTIONGestionBundle:Documento:filtro.html.twig', array(
				'tipo'=>'gastos',
			));
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }


    public function informesAction(Request $request)
    {
		if($this->sessionSvc->isLogged()){
			$desde = new \DateTime( $request->get('fechaDesde') );
			$hasta = new \DateTime( $request->get('fechaHasta') );
			$saldoP = 0;
			$porcP = 0;
	
			$em = $this->getDoctrine()->getManager();
			// ask the service for a Excel5
			$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
	
			$phpExcelObject->getProperties()->setCreator("Sublink")
			   ->setLastModifiedBy("Sublink")
			   ->setTitle("Informe")
			   ->setSubject("Sublink");
	
			$phpExcelObject->setActiveSheetIndex(0)
			   ->setCellValue('A1', 'Fecha: '.date('d-m-Y'))
			   ->setCellValue('A2', 'Periodo')
			   ->setCellValue('B2', 'Desde: '.$desde->format('d-m-Y'))
			   ->setCellValue('C2', 'Hasta: '.$hasta->format('d-m-Y'));
			   				
			$documentos = $em->getRepository('GESTIONGestionBundle:Documento')->findBy(array('estado'=>'A', 'tipoDocumento'=>'G'));

			$count = 4;
			if($request->get('agrupado')=='1'){	
				$phpExcelObject->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
				$phpExcelObject->getActiveSheet()->setTitle('Informe de Gastos');
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A3', 'Dia/Mes')
				   ->setCellValue('B3', '% del total del periodo')
				   ->setCellValue('C3', 'Gastos ($)')
				   ->setCellValue('D3', 'Tipo Gasto')
				   ;			
	
				$fechaAnt = '';
				$tipoGasto = '';
				$valorAntP = 0;
				$valorAntD = 0;
				foreach($documentos as $doc){
					if($desde <= $doc->getFecha() and $doc->getFecha() <= $hasta ){
						$saldoP = $saldoP + $doc->getImporte();
						if($fechaAnt == ''){
							$fechaAnt = $doc->getFecha()->format('d-m-Y');
							$tipoGasto = $doc->getTipoGasto();
							$valorAntP = $valorAntP + $doc->getImporte();
							$porcP = ($valorAntP * 100) / $saldoP;
						}else{						
							if($fechaAnt == $doc->getFecha()->format('d-m-Y')){
								$fechaAnt = $doc->getFecha()->format('d-m-Y');
								$valorAntP = $valorAntP + $doc->getImporte();
								$porcP = ($valorAntP * 100) / $saldoP;
							}else{		
								$phpExcelObject->setActiveSheetIndex(0)
								   ->setCellValue('A'.$count, $fechaAnt)
								   ->setCellValue('B'.$count, round($porcP, 2))
								   ->setCellValue('C'.$count, $valorAntP)
								   ->setCellValue('D'.$count, $tipoGasto);		
								$valorAntP = 0;
								$porcP = 0;
								$valorAntP = $valorAntP + $doc->getImporte();
								$porcP = ($valorAntP * 100) / $saldoP;

								$fechaAnt = $doc->getFecha()->format('d-m-Y');
								$count = $count + 1;
							}
						}
					}
				}
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A'.$count, $fechaAnt)
				   ->setCellValue('B'.$count, round($porcP, 2))
				   ->setCellValue('C'.$count, $valorAntP)
				   ->setCellValue('D'.$count, $tipoGasto);		
				$count = $count + 1;
			}else{
	// AGRUPAMIENTO MES
				$phpExcelObject->getActiveSheet()->setTitle('Informe de Gastos');
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A3', 'Dia/Mes')
				   ->setCellValue('B3', '% del total del periodo')
				   ->setCellValue('C3', 'Gastos ($)');
	
				$fechaAnt = '';
				$valorAntP = 0;
				$valorAntD = 0;
				foreach($documentos as $doc){
					if($desde <= $doc->getFecha() and $doc->getFecha() < $hasta ){
						$saldoP = $saldoP + $doc->getImporte();
						if($fechaAnt == ''){
							$fechaAnt = $doc->getFecha()->format('m-Y');
							$valorAntP = $valorAntP + $doc->getImporte();
							$porcP = ($valorAntP * 100) / $saldoP;
						}else{						
							if($fechaAnt == $doc->getFecha()->format('m-Y')){
								$fechaAnt = $doc->getFecha()->format('m-Y');
								$valorAntP = $valorAntP + $doc->getImporte();
								$porcP = ($valorAntP * 100) / $saldoP;
							}else{		
								$phpExcelObject->setActiveSheetIndex(0)
								   ->setCellValue('A'.$count, $fechaAnt)
								   ->setCellValue('B'.$count, round($porcP, 2))
								   ->setCellValue('C'.$count, $valorAntP);		
								$valorAntP = 0;
								$porcP = 0;
								$valorAntP = $valorAntP + $doc->getImporte();
								$porcP = ($valorAntP * 100) / $saldoP;

								$fechaAnt = $doc->getFecha()->format('m-Y');
								$count = $count + 1;
							}
						}
					}
				}
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A'.$count, $fechaAnt)
				   ->setCellValue('B'.$count, round($porcP, 2))
				   ->setCellValue('C'.$count, $valorAntP);		
				$count = $count + 1;
			}
			$phpExcelObject->setActiveSheetIndex(0)
			   ->setCellValue('A'.$count, 'TOTALES')
			   ->setCellValue('B'.$count, '100%')
			   ->setCellValue('C'.$count, '$'.$saldoP);		
	
			$phpExcelObject->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
	
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$phpExcelObject->setActiveSheetIndex(0);
			$phpExcelObject->getActiveSheet()->getStyle('A'.$count.':D'.$count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
			$phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
			$phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
			$phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(25);
			$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
			// create the response
			$response = $this->get('phpexcel')->createStreamedResponse($writer);
			// adding headers
			$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
			$response->headers->set('Content-Disposition', 'attachment;filename=InformeGastos.xls');
			
			$response->headers->set('Pragma', 'public');
			$response->headers->set('Cache-Control', 'maxage=1');
			
			return $response;
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }
	
    public function informeDeEntregasAction(Request $request)
    {
		if($this->sessionSvc->isLogged()){
			$desde = new \DateTime( $request->get('fechaDesde') );
			$hasta = new \DateTime( $request->get('fechaHasta') );
			$saldoP = 0;
			$porcP = 0;
	
			$em = $this->getDoctrine()->getManager();
			// ask the service for a Excel5
			$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
	
			$phpExcelObject->getProperties()->setCreator("Sublink")
			   ->setLastModifiedBy("Sublink")
			   ->setTitle("Informe de Entregas")
			   ->setSubject("Sublink");
	
			$phpExcelObject->setActiveSheetIndex(0)
			   ->setCellValue('A1', 'Fecha: '.date('d-m-Y'))
			   ->setCellValue('A2', 'Periodo')
			   ->setCellValue('B2', 'Desde: '.$desde->format('d-m-Y'))
			   ->setCellValue('C2', 'Hasta: '.$hasta->format('d-m-Y'));
			   				
			$documentos = $em->getRepository('GESTIONGestionBundle:Colaboracion')->filtro($desde, $hasta, "ASC");

			$count = 4;
			if($request->get('agrupado')=='1'){	
				$phpExcelObject->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
				$phpExcelObject->getActiveSheet()->setTitle('Informe de Entregas');
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A3', 'Dia/Mes')
				   ->setCellValue('B3', '% del total del periodo')
				   ->setCellValue('C3', 'Entregas ($)')
				   ;			
	
				$fechaAnt = '';
				$cliente = '';
				$valorAntP = 0;
				$valorAntD = 0;
				foreach($documentos as $doc){
					$saldoP = $saldoP + $doc->getTotal();
					if($fechaAnt == ''){
						$fechaAnt = $doc->getFecha()->format('d-m-Y');
						$cliente = $doc->getCliente();
						$valorAntP = $valorAntP + $doc->getTotal();
						$porcP = ($valorAntP * 100) / $saldoP;
					}else{						
						if($fechaAnt == $doc->getFecha()->format('d-m-Y')){
							$fechaAnt = $doc->getFecha()->format('d-m-Y');
							$valorAntP = $valorAntP + $doc->getTotal();
							$porcP = ($valorAntP * 100) / $saldoP;
						}else{		
							$phpExcelObject->setActiveSheetIndex(0)
							   ->setCellValue('A'.$count, $fechaAnt)
							   ->setCellValue('B'.$count, round($porcP, 2))
							   ->setCellValue('C'.$count, $valorAntP);		
							$valorAntP = 0;
							$porcP = 0;
							$valorAntP = $valorAntP + $doc->getTotal();
							$porcP = ($valorAntP * 100) / $saldoP;

							$fechaAnt = $doc->getFecha()->format('d-m-Y');
							$count = $count + 1;
						}
					}
				}
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A'.$count, $fechaAnt)
				   ->setCellValue('B'.$count, round($porcP, 2))
				   ->setCellValue('C'.$count, $valorAntP);		
				$count = $count + 1;
			}else{
	// AGRUPAMIENTO MES
				$phpExcelObject->getActiveSheet()->setTitle('Informe de Gastos');
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A3', 'Dia/Mes')
				   ->setCellValue('B3', '% del total del periodo')
				   ->setCellValue('C3', 'Entregas ($)');
	
				$fechaAnt = '';
				$valorAntP = 0;
				$valorAntD = 0;
				foreach($documentos as $doc){
					$saldoP = $saldoP + $doc->getTotal();
					if($fechaAnt == ''){
						$fechaAnt = $doc->getFecha()->format('m-Y');
						$valorAntP = $valorAntP + $doc->getTotal();
						$porcP = ($valorAntP * 100) / $saldoP;
					}else{						
						if($fechaAnt == $doc->getFecha()->format('m-Y')){
							$fechaAnt = $doc->getFecha()->format('m-Y');
							$valorAntP = $valorAntP + $doc->getTotal();
							$porcP = ($valorAntP * 100) / $saldoP;
						}else{		
							$phpExcelObject->setActiveSheetIndex(0)
							   ->setCellValue('A'.$count, $fechaAnt)
							   ->setCellValue('B'.$count, round($porcP, 2))
							   ->setCellValue('C'.$count, $valorAntP);		
							$valorAntP = 0;
							$porcP = 0;
							$valorAntP = $valorAntP + $doc->getTotal();
							$porcP = ($valorAntP * 100) / $saldoP;

							$fechaAnt = $doc->getFecha()->format('m-Y');
							$count = $count + 1;
						}
					}
				}
				$phpExcelObject->setActiveSheetIndex(0)
				   ->setCellValue('A'.$count, $fechaAnt)
				   ->setCellValue('B'.$count, round($porcP, 2))
				   ->setCellValue('C'.$count, $valorAntP);		
				$count = $count + 1;
			}
			$phpExcelObject->setActiveSheetIndex(0)
			   ->setCellValue('A'.$count, 'TOTALES')
			   ->setCellValue('B'.$count, '100%')
			   ->setCellValue('C'.$count, '$'.$saldoP);		
	
			$phpExcelObject->getActiveSheet()->getStyle('A3:C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
	
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$phpExcelObject->setActiveSheetIndex(0);
			$phpExcelObject->getActiveSheet()->getStyle('A'.$count.':C'.$count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
			$phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
			$phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
			$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
			// create the response
			$response = $this->get('phpexcel')->createStreamedResponse($writer);
			// adding headers
			$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
			$response->headers->set('Content-Disposition', 'attachment;filename=InformeEntregas.xls');
			
			$response->headers->set('Pragma', 'public');
			$response->headers->set('Cache-Control', 'maxage=1');
			
			return $response;
	    }else{
			return $this->redirect($this->generateUrl('_homepage'));
		}
    }
	
	
    public function facturaproveedorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Documento();
		$movimientocc = new MovimientoCC();
		$entity->setTipoGasto(NULL);
		$valor = 0;
		$fecha = new \DateTime($request->get('fechaDocumento'));
		$unidad = $em->getRepository('GESTIONGestionBundle:UnidadNegocio')->find($request->get('unidad'));			
		$cliPro = $em->getRepository('GESTIONGestionBundle:ClienteProveedor')->find($request->get('proveedor'));

		$movimientocc->setUnidadNegocio($unidad);			
		$movimientocc->setClienteProveedor($cliPro);            
		$movimientocc->setDocumento($entity);
		$movimientocc->setTipoDocumento('F');
		$movimientocc->setMoneda($request->get('moneda'));

		$entity->setMoneda($request->get('moneda'));
		$entity->setDescuento($request->get('descuento'));
		$entity->setFecha($fecha);
		$entity->setBonificacion($request->get('bonificacion'));
		$entity->setTipoDocumento('F');
	
		$entity->setImporte($valor);

		$em->persist($entity);
		$em->flush();
		$em->persist($movimientocc);
		$em->flush();

		$entity->setMovimientocc($movimientocc);

		$contador = $request->get('contador');		
		
		for($x = 0; $x <= $contador; $x++){
			$codigo = $request->get('codigo'.$x);
			$desc = $request->get('descripcion'.$x);
			$precio = $request->get('precio'.$x);
			$cantidad = $request->get('cantidad'.$x);
			
			if(!is_null($codigo) and $codigo!='' and $desc != '' and !is_null($desc) and $precio != '' and !is_null($precio)and $cantidad != '' and !is_null($cantidad) ){

				$productoDocumento = new ProductoDocumento();
				
				if($codigo=='000'){
					$producto = $em->getRepository('GESTIONGestionBundle:Producto')->findOneBy(array('codigo' => $codigo));					
					$productoDocumento->setProducto($producto);
					$productoDocumento->setDocumento($entity);				
					$productoDocumento->setPrecio($request->get('precio'.$x));				
					$productoDocumento->setCantidad($request->get('cantidad'.$x));				
					$entity->addProductosDocumento($productoDocumento);
				}else{
					$producto = $em->getRepository('GESTIONGestionBundle:Producto')->findOneBy(array('codigo' => $codigo));					
					
					if(is_null($producto)){
						$producto = new Producto();

						$producto->setCodigo($codigo);
						$producto->setDescripcion($desc);
						$producto->setMoneda($request->get('moneda'));

                        $stockunidad = $em->getRepository('GESTIONGestionBundle:StockUnidad')
                                    ->findOneBy(array('producto' => $producto->getId(), 
                                                      'unidad'=>$request->get('unidad')));
                        if(is_null($stockunidad)){
                            $stockunidad = new StockUnidad();
                            $stockunidad->setProducto($producto);
                            $stockunidad->setUnidad($unidad);
                            $stockunidad->setStock($request->get('cantidad'.$x));
                            $producto->addStockunidade($stockunidad);
                            $em->persist($stockunidad);
                            $em->persist($producto);
                        }else{
                            $stockunidad->setStock($request->get('cantidad'.$x));
                        }

						$em->persist($producto);
						$producto->setCosto($request->get('precio'.$x));					
					}else{
                        $stockunidad = $em->getRepository('GESTIONGestionBundle:StockUnidad')
                                    ->findOneBy(array('producto' => $producto->getId(), 
                                                      'unidad'=>$request->get('unidad')));
                        if(is_null($stockunidad)){
                            $stockunidad = new StockUnidad();
                            $stockunidad->setProducto($producto);
                            $stockunidad->setUnidad($unidad);
                            $stockunidad->setStock($request->get('cantidad'.$x));
                            $producto->addStockunidade($stockunidad);
                            $em->persist($stockunidad);
                            $em->persist($producto);
                        }else{
                            $stock = $stockunidad->getStock() + $request->get('cantidad'.$x);
                        }
                        $stockunidad->setStock($stock);
						if($producto->getMoneda()==$request->get('moneda')){
							$producto->setCosto($request->get('precio'.$x));					
						}
					}

					$productoDocumento->setProducto($producto);
					$productoDocumento->setDocumento($entity);
					$productoDocumento->setPrecio($request->get('precio'.$x));			
					$productoDocumento->setCantidad($request->get('cantidad'.$x));
					
					$entity->addProductosDocumento($productoDocumento);
					$em->persist($productoDocumento);
				}

				$valor = $valor + ($precio*$cantidad);
				$em->flush();
			}
		}	
		
		if(count($entity->getProductosDocumento())==0){
			$em->remove($movimientocc);
			$em->flush();
			$em->remove($entity);
			$em->flush();
			$this->sessionSvc->addFlash('msgError','No puede generar una factura sin productos.');
			return $this->redirect($this->generateUrl('documento_compras'));
		}

		$entity->setImporte($valor);
		$em->flush();

		$this->sessionSvc->addFlash('msgOk','Compra registrada.');
		return $this->redirect($this->generateUrl('documento_show', array('id'=>$entity->getId())));
    }

}

