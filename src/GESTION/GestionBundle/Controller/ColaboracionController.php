<?php

namespace GESTION\GestionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GESTION\GestionBundle\Entity\Colaboracion;
use GESTION\GestionBundle\Entity\ElementoColaboracion;
use GESTION\GestionBundle\Entity\Adjunto;
use GESTION\GestionBundle\Form\ColaboracionType;
use GESTION\GestionBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

// IMPORTACION NECESARIA PARA ARCHIVOS
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Colaboracion controller.
 *
 */
class ColaboracionController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    /**
     * Lists all Colaboracion entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

		if(empty($request->get('desde'))){
			$desde = new \DateTime('NOW -30 days');
		}else{
			$desde = new \DateTime($request->get('desde'));
		}

		if(empty($request->get('hasta'))){
			$hasta = new \DateTime('NOW +1 days');
		}else{
			$hasta = new \DateTime($request->get('hasta'));
		}

        $entities = $em->getRepository('GESTIONGestionBundle:Colaboracion')->filtro($desde, $hasta);

        return $this->render('GESTIONGestionBundle:Colaboracion:index.html.twig', array(
            'entities' => $entities,
            'desde' => $desde->format('Y-m-d'),
            'hasta' => $hasta->format('Y-m-d'),
        ));
    }
    /**
     * Creates a new Colaboracion entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Colaboracion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

		$fechastr = $form->get("fecha")->getData();

		$clientestr = $form->get("idcliente")->getData();
		if(!empty($clientestr)){
			$cliente = $em->getRepository('GESTIONGestionBundle:Cliente')->find($clientestr);
			$entity->setCliente($cliente);
		}

		if(!empty($fechastr)){
			$entity->setFecha(new \DateTime($fechastr));
		}
		$total = 0;
		if ($form->isValid()) {
			for($i = 1; $i <=30; $i++ ){
				$idelemento = $request->get("idelemento$i");
				if(!empty($idelemento)){
					$elementostocks = $em->getRepository('GESTIONGestionBundle:ElementoStock')->findBy(array('elemento'=>$idelemento), array('fecha'=>'ASC'));
					$cantidad = $request->get("cantidad$i");
					$precio = $request->get("precio$i");

					$total+= $cantidad*$precio;

					foreach($elementostocks as $elementostock){
						if($cantidad > 0 and $cantidad<=$elementostock->getStock()){
							$elementocolaboracion = new ElementoColaboracion();
							$elementocolaboracion->setElementostock($elementostock);
							$elementocolaboracion->setColaboracion($entity);
							$elementocolaboracion->setCantidad($cantidad);
							$elementocolaboracion->setPrecio($precio);
							$stock = $elementostock->getStock() - $cantidad;
							$elementostock->setStock($stock);
							$em->persist($elementocolaboracion);
							$cantidad = 0;
						}else{
							if($cantidad > 0 and $elementostock->getStock()>0){
								$elementocolaboracion = new ElementoColaboracion();
								$elementocolaboracion->setElementostock($elementostock);
								$elementocolaboracion->setColaboracion($entity);
								$elementocolaboracion->setPrecio($precio);

								$elementocolaboracion->setCantidad($elementostock->getStock());
								$cantidad = $cantidad - $elementostock->getStock();
								$elementostock->setStock(0);
								$em->persist($elementocolaboracion);
							}
						}
					}
				}
			}
			for($i = 1; $i <=9; $i++ ){
				$file = $request->files->get('adjunto'.$i);
				if($file){
					if($file->getSize()>0){
						$strm = fopen($file->getRealPath(),'rb');

						$adjunto = new Adjunto();
						$adjunto->setNombre($file->getClientOriginalName());
						$adjunto->setContenido('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
						$adjunto->setColaboracion($entity);
						$em->persist($adjunto);
					}
				}
			}

			if($entity->getDescuento()!='' and $entity->getDescuento()!=0){
				$total = $total * (1-($entity->getDescuento()/100));
			}

			if($entity->getBonificacion()!='' and $entity->getBonificacion()>0){
				$total-=$entity->getBonificacion();
			}

			$entity->setTotal($total);

            $em->persist($entity);

            $em->flush();

            return $this->redirect($this->generateUrl('colaboracion'));
       }

        return $this->render('GESTIONGestionBundle:Colaboracion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Colaboracion entity.
     *
     * @param Colaboracion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Colaboracion $entity)
    {
        $form = $this->createForm(new ColaboracionType(), $entity, array(
            'action' => $this->generateUrl('colaboracion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=>array('class'=>'btn btn-primary', 'style'=>'float: right')));

        return $form;
    }

    /**
     * Displays a form to create a new Colaboracion entity.
     *
     */
    public function newAction()
    {
        $entity = new Colaboracion();
        $form   = $this->createCreateForm($entity);

        return $this->render('GESTIONGestionBundle:Colaboracion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Colaboracion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colaboracion entity.');
        }

        return $this->render('GESTIONGestionBundle:Colaboracion:show.html.twig', array(
            'entity'      	=> $entity,
        ));
    }

    /**
     * Displays a form to edit an existing Colaboracion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colaboracion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GESTIONGestionBundle:Colaboracion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function parcialAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

        return $this->render('GESTIONGestionBundle:Colaboracion:parcial.html.twig', array(
            'entity'      => $entity,
        ));
    }

    public function hacerparcialAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);
		$concluido = true;
		foreach($entity->getElementoscolaboracion() as $elementocolaboracion){
			$cantidad = $request->get("cantidad".$elementocolaboracion->getId());
			if($cantidad>0){
				$nuevacantidad = $elementocolaboracion->getCantidadparcial()+$cantidad;
				$elementocolaboracion->setCantidadparcial($nuevacantidad);
				$stock = $elementocolaboracion->getElemento()->getStock() + $cantidad;
				$elementocolaboracion->getElemento()->setStock($stock);
			}

			if($elementocolaboracion->getCantidad()>$elementocolaboracion->getCantidadparcial()){
				$concluido = false;
			}
		}

		if($concluido){
			$entity->setCodEstado("R");
		}else{
			$entity->setCodEstado("P");
		}
		$em->flush();

		$this->sessionManager->addFlash("msgOk", "Entrega Parcial exitosa.");

		return $this->redirect($this->generateUrl('colaboracion'));
    }

    /**
    * Creates a form to edit a Colaboracion entity.
    *
    * @param Colaboracion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Colaboracion $entity)
    {
        $form = $this->createForm(new ColaboracionType(), $entity, array(
            'action' => $this->generateUrl('colaboracion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=>array('class'=>'btn btn-primary', 'style'=>'float: right')));

        return $form;
    }
    /**
     * Edits an existing Colaboracion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

		$fechastr = $editForm->get("fecha")->getData();

		if(!empty($fechastr)){
			$entity->setFecha(new \DateTime($fechastr));
		}else{
			$entity->setFecha(null);
		}

		$total = 0;
        if ($editForm->isValid()) {
			for($i = 1; $i <=30; $i++ ){
				$idelemento = $request->get("idelemento$i");
				if(!empty($idelemento)){
					$idelementocolaboracion = $request->get("idelementocolaboracion$idelemento");
					if(!empty($idelementocolaboracion)){
						$elementocolaboracion = $em->getRepository('GESTIONGestionBundle:ElementoColaboracion')->find($idelementocolaboracion);
						$elementocolaboracion->setPrecio($request->get("precio$i"));

						$total+= $request->get("cantidad$i")*$request->get("precio$i");

						if($request->get("cantidad$i")>0){
							$cantidad = $elementocolaboracion->getCantidad()-$request->get("cantidad$i");
							if($cantidad>0){
								$elementocolaboracion->setCantidad($request->get("cantidad$i"));
								$stock = $elementocolaboracion->getElementostock()->getStock() + $cantidad;
								$elementocolaboracion->getElementostock()->setStock($stock);
							}else{
								if($cantidad<0){
									$elementocolaboracion->setCantidad($request->get("cantidad$i"));
									$stock = $elementocolaboracion->getElementostock()->getStock() + $cantidad;
									$elementocolaboracion->getElementostock()->setStock($stock);
								}
							}
						}else{
							$stock = $elementocolaboracion->getElementostock()->getStock() + $elementocolaboracion->getCantidad();
							$elementocolaboracion->getElementostock()->setStock($stock);
							$em->remove($elementocolaboracion);
						}

						$em->flush();
					}else{
						$elemento = $em->getRepository('GESTIONGestionBundle:Elemento')->find($idelemento);
						if($elemento){
							$elementocolaboracion = new ElementoColaboracion();
							$elementocolaboracion->setElemento($elemento);
							$elementocolaboracion->setColaboracion($entity);
							$elementocolaboracion->setCantidad($request->get("cantidad$i"));
							$elementocolaboracion->setPrecio($request->get("precio$i"));

							$stock = $elemento->getStock() - $request->get("cantidad$i");
							$elemento->setStock($stock);
							$em->persist($elementocolaboracion);
						}
					}
				}
			}
			for($i = 1; $i <=9; $i++ ){
				$file = $request->files->get('adjunto'.$i);
				if($file){
					if($file->getSize()>0){
						$strm = fopen($file->getRealPath(),'rb');

						$adjunto = new Adjunto();
						$adjunto->setNombre($file->getClientOriginalName());
						$adjunto->setContenido('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
						$adjunto->setColaboracion($entity);
						$em->persist($adjunto);
					}
				}
			}
			$checks = $request->get('adjuntos');

			if(!empty($checks)){
				foreach ($checks as $check){
					$adjunto = $em->getRepository('GESTIONGestionBundle:Adjunto')->find($check);
					$em->remove($adjunto);
				}
			}

			if($entity->getDescuento()!='' and $entity->getDescuento()!=0){
				$total = $total * (1-($entity->getDescuento()/100));
			}

			if($entity->getBonificacion()!='' and $entity->getBonificacion()>0){
				$total-=$entity->getBonificacion();
			}

			$entity->setTotal($total);
            $em->flush();

            return $this->redirect($this->generateUrl('colaboracion'));
        }

        return $this->render('GESTIONGestionBundle:Colaboracion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Colaboracion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

		foreach($entity->getElementoscolaboracion() as $elementocolaboracion){
			$stock = $elementocolaboracion->getElementostock()->getStock() + $elementocolaboracion->getCantidad();
			$elementocolaboracion->getElementostock()->setStock($stock);
		}
		$entity->setCodEstado("E");
		$em->flush();

		$this->sessionManager->addFlash("msgOk", "Entrega eliminada con éxito.");

		return $this->redirect($this->generateUrl('colaboracion'));
    }

    public function concluirAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

		foreach($entity->getElementoscolaboracion() as $elementocolaboracion){
			$stock = $elementocolaboracion->getElemento()->getStock() + ($elementocolaboracion->getCantidad()-$elementocolaboracion->getCantidadparcial());
			$elementocolaboracion->getElemento()->setStock($stock);
		}
		$entity->setCodEstado("R");
		$em->flush();

		$this->sessionManager->addFlash("msgOk", "Colaboración eliminada con éxito.");

		return $this->redirect($this->generateUrl('colaboracion'));
    }

    public function activarAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);
		$entity->setCodestado("A");
		$em->flush();

        return $this->redirect($this->generateUrl('colaboracion'));
    }

    public function cancelarAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);
		$entity->setCodestado("C");
		$em->flush();

        return $this->redirect($this->generateUrl('colaboracion'));
    }

    public function reportarpagoAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

		$entity->setCodestado("PNE");
		$em->flush();

        return $this->redirect($this->generateUrl('colaboracion'));
    }

    public function reportarentregaAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

		foreach($entity->getElementoscolaboracion() as $elementocolaboracion){
			$elementostocks = $em->getRepository('GESTIONGestionBundle:ElementoStock')->findBy(array('elemento'=>$elementocolaboracion->getElementostock()->getElemento()->getId()), array('fecha'=>'ASC'));

			$cantidad = $elementocolaboracion->getCantidad();

			if($cantidad > $elementocolaboracion->getElementostock()->getElemento()->getStock()){
				$this->sessionManager->addFlash('msgError', 'No hay stock del producto '.$elementocolaboracion->getElementostock()->getElemento()->getNombre().' para poder completar esta entrega');
				return $this->redirect($this->generateUrl('colaboracion'));
			}

			foreach($elementostocks as $elementostock){
				if($cantidad > 0 and $cantidad<=$elementostock->getStock()){
					$elementocolaboracion->setElementostock($elementostock);
					$stock = $elementostock->getStock() - $cantidad;
					$elementostock->setStock($stock);

					$cantidad = 0;
				}else{
					if($cantidad > 0 and $elementostock->getStock()>0){
						$elementocolaboracion = new ElementoColaboracion();
						$elementocolaboracion->setElementostock($elementostock);
						$elementocolaboracion->setColaboracion($entity);
						$elementocolaboracion->setPrecio($precio);

						$elementocolaboracion->setCantidad($elementostock->getStock());
						$cantidad = $cantidad - $elementostock->getStock();
						$elementostock->setStock(0);
						$em->persist($elementocolaboracion);
					}
				}
			}
		}

		$entity->setCodestado("PE");
		$em->flush();

        return $this->redirect($this->generateUrl('colaboracion'));
    }

    /**
     * Creates a form to delete a Colaboracion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('colaboracion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
