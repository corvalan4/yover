<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // _index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_index');
            }

            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::indexAction',  '_route' => '_index',);
        }

        // _saludo
        if ($pathinfo === '/saludo') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::saludoAction',  '_route' => '_saludo',);
        }

        if (0 === strpos($pathinfo, '/re')) {
            // _redireccion
            if ($pathinfo === '/redireccion') {
                return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::redireccionAction',  '_route' => '_redireccion',);
            }

            // _render
            if ($pathinfo === '/render') {
                return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::renderAction',  '_route' => '_render',);
            }

        }

        // _limpiar
        if ($pathinfo === '/limpiar') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::limpiarAction',  '_route' => '_limpiar',);
        }

        // _contacto
        if ($pathinfo === '/contacto') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::contactoAction',  '_route' => '_contacto',);
        }

        // _nosotros
        if ($pathinfo === '/nosotros') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::nosotrosAction',  '_route' => '_nosotros',);
        }

        // _ingresar
        if ($pathinfo === '/ingresar') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::ingresarAction',  '_route' => '_ingresar',);
        }

        // _login_web
        if ($pathinfo === '/login-cliente') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::loginAction',  '_route' => '_login_web',);
        }

        // _registro
        if ($pathinfo === '/registro') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::registroAction',  '_route' => '_registro',);
        }

        // _salir
        if ($pathinfo === '/salir') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::salirAction',  '_route' => '_salir',);
        }

        // _mispedidos
        if ($pathinfo === '/mispedidos') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::mispedidosAction',  '_route' => '_mispedidos',);
        }

        // _verpedido
        if (0 === strpos($pathinfo, '/verpedido') && preg_match('#^/verpedido/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_verpedido')), array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::verpedidoAction',));
        }

        // _misdatos
        if ($pathinfo === '/misdatos') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::misdatosAction',  '_route' => '_misdatos',);
        }

        // _categoria
        if (0 === strpos($pathinfo, '/categoria') && preg_match('#^/categoria/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_categoria')), array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::categoriaAction',));
        }

        // _producto
        if (0 === strpos($pathinfo, '/producto') && preg_match('#^/producto/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_producto')), array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::productoAction',));
        }

        // _update_cliente
        if (0 === strpos($pathinfo, '/updateCliente') && preg_match('#^/updateCliente/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_update_cliente')), array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::updateClienteAction',));
        }

        // _novedades
        if ($pathinfo === '/novedades') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::novedadesAction',  '_route' => '_novedades',);
        }

        // _tienda
        if (0 === strpos($pathinfo, '/tienda') && preg_match('#^/tienda(?:/(?P<categoria>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_tienda')), array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::tiendaAction',  'categoria' => 'todo',));
        }

        // _checkout
        if ($pathinfo === '/checkout') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::checkoutAction',  '_route' => '_checkout',);
        }

        // _agregar
        if ($pathinfo === '/agregar') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::agregarAction',  '_route' => '_agregar',);
        }

        // _quitar
        if ($pathinfo === '/quitar') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::quitarAction',  '_route' => '_quitar',);
        }

        // _vaciar
        if ($pathinfo === '/vaciar') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::vaciarAction',  '_route' => '_vaciar',);
        }

        if (0 === strpos($pathinfo, '/re')) {
            // _realizarpedido
            if ($pathinfo === '/realizarpedido') {
                return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::realizarpedidoAction',  '_route' => '_realizarpedido',);
            }

            // _confirmarmediopago
            if (0 === strpos($pathinfo, '/respuestapago') && preg_match('#^/respuestapago/(?P<id>[^/]++)/(?P<estado>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_confirmarmediopago')), array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::respuestapagoAction',));
            }

        }

        // _elegirmedio
        if (0 === strpos($pathinfo, '/elegirmedio') && preg_match('#^/elegirmedio/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_elegirmedio')), array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::elegirmedioAction',));
        }

        // _mail
        if ($pathinfo === '/mail') {
            return array (  '_controller' => 'GESTION\\TiendaBundle\\Controller\\DefaultController::mailAction',  '_route' => '_mail',);
        }

        if (0 === strpos($pathinfo, '/gestion')) {
            // _homepage
            if (rtrim($pathinfo, '/') === '/gestion') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_homepage');
                }

                return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::indexAction',  '_route' => '_homepage',);
            }

            // _inicio
            if ($pathinfo === '/gestion/inicio') {
                return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::inicioAction',  '_route' => '_inicio',);
            }

            // _cerrar
            if ($pathinfo === '/gestion/cerrar') {
                return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::cerrarAction',  '_route' => '_cerrar',);
            }

            // _login
            if ($pathinfo === '/gestion/login') {
                return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::loginAction',  '_route' => '_login',);
            }

            if (0 === strpos($pathinfo, '/gestion/ganancia')) {
                // _ganancias
                if ($pathinfo === '/gestion/ganancias') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::gananciasAction',  '_route' => '_ganancias',);
                }

                // _gananciaventas
                if ($pathinfo === '/gestion/gananciaventas') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::gananciaventasAction',  '_route' => '_gananciaventas',);
                }

            }

            if (0 === strpos($pathinfo, '/gestion/predictiva')) {
                // _predictiva
                if ($pathinfo === '/gestion/predictiva') {
                    return array (  '_controller' => 'PFAPfaBundle:Default:predictiva',  '_route' => '_predictiva',);
                }

                // _predictivaPropia
                if ($pathinfo === '/gestion/predictivaPropia') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::predictivaPropiaAction',  '_route' => '_predictivaPropia',);
                }

            }

            // informeingresosegresos
            if ($pathinfo === '/gestion/informeingresosegresos') {
                return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::informeingresosegresosAction',  '_route' => 'informeingresosegresos',);
            }

            // _cargamasiva
            if ($pathinfo === '/gestion/cargamasiva') {
                return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::cargamasivaAction',  '_route' => '_cargamasiva',);
            }

            // _importarArchivos
            if ($pathinfo === '/gestion/importarArchivos') {
                return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DefaultController::importarArchivosAction',  '_route' => '_importarArchivos',);
            }

            if (0 === strpos($pathinfo, '/gestion/elemento')) {
                // elemento
                if (rtrim($pathinfo, '/') === '/gestion/elemento') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'elemento');
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::indexAction',  '_route' => 'elemento',);
                }

                if (0 === strpos($pathinfo, '/gestion/elemento/listado')) {
                    // elemento_listado
                    if ($pathinfo === '/gestion/elemento/listado') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::listadoAction',  '_route' => 'elemento_listado',);
                    }

                    // elemento_listadocategoria
                    if ($pathinfo === '/gestion/elemento/listadocategoria') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::listadocategoriaAction',  '_route' => 'elemento_listadocategoria',);
                    }

                }

                // elemento_show
                if (preg_match('#^/gestion/elemento/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'elemento_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::showAction',));
                }

                // elemento_new
                if ($pathinfo === '/gestion/elemento/new') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::newAction',  '_route' => 'elemento_new',);
                }

                // elemento_create
                if ($pathinfo === '/gestion/elemento/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_elemento_create;
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::createAction',  '_route' => 'elemento_create',);
                }
                not_elemento_create:

                // elemento_edit
                if (preg_match('#^/gestion/elemento/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'elemento_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::editAction',));
                }

                // elemento_update
                if (preg_match('#^/gestion/elemento/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'elemento_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::updateAction',));
                }

                // elemento_activar
                if (preg_match('#^/gestion/elemento/(?P<id>[^/]++)/activar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'elemento_activar')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::activarAction',));
                }

                // elemento_visible
                if (preg_match('#^/gestion/elemento/(?P<id>[^/]++)/visible$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'elemento_visible')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::visibleAction',));
                }

                // elemento_novisible
                if (preg_match('#^/gestion/elemento/(?P<id>[^/]++)/novisible$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'elemento_novisible')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::novisibleAction',));
                }

                // elemento_delete
                if (preg_match('#^/gestion/elemento/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'elemento_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoController::deleteAction',));
                }

                if (0 === strpos($pathinfo, '/gestion/elementostock')) {
                    // elementostock
                    if (0 === strpos($pathinfo, '/gestion/elementostock/idelemento') && preg_match('#^/gestion/elementostock/idelemento\\=(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'elementostock')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoStockController::indexAction',));
                    }

                    // elementostock_show
                    if (preg_match('#^/gestion/elementostock/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'elementostock_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoStockController::showAction',));
                    }

                    // elementostock_new
                    if (preg_match('#^/gestion/elementostock/(?P<id>[^/]++)/new$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'elementostock_new')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoStockController::newAction',));
                    }

                    // elementostock_create
                    if ($pathinfo === '/gestion/elementostock/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_elementostock_create;
                        }

                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoStockController::createAction',  '_route' => 'elementostock_create',);
                    }
                    not_elementostock_create:

                    // elementostock_edit
                    if (preg_match('#^/gestion/elementostock/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'elementostock_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoStockController::editAction',));
                    }

                    // elementostock_update
                    if (preg_match('#^/gestion/elementostock/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_elementostock_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'elementostock_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoStockController::updateAction',));
                    }
                    not_elementostock_update:

                    // elementostock_delete
                    if (preg_match('#^/gestion/elementostock/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_elementostock_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'elementostock_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ElementoStockController::deleteAction',));
                    }
                    not_elementostock_delete:

                }

            }

            if (0 === strpos($pathinfo, '/gestion/c')) {
                if (0 === strpos($pathinfo, '/gestion/categoria')) {
                    // categoria
                    if (rtrim($pathinfo, '/') === '/gestion/categoria') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'categoria');
                        }

                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::indexAction',  '_route' => 'categoria',);
                    }

                    // categoria_show
                    if (preg_match('#^/gestion/categoria/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'categoria_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::showAction',));
                    }

                    // categoria_new
                    if ($pathinfo === '/gestion/categoria/new') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::newAction',  '_route' => 'categoria_new',);
                    }

                    // categoria_create
                    if ($pathinfo === '/gestion/categoria/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_categoria_create;
                        }

                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::createAction',  '_route' => 'categoria_create',);
                    }
                    not_categoria_create:

                    // categoria_edit
                    if (preg_match('#^/gestion/categoria/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'categoria_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::editAction',));
                    }

                    // categoria_update
                    if (preg_match('#^/gestion/categoria/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_categoria_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'categoria_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::updateAction',));
                    }
                    not_categoria_update:

                    // categoria_activar
                    if (preg_match('#^/gestion/categoria/(?P<id>[^/]++)/activar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'categoria_activar')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::activarAction',));
                    }

                    // categoria_delete
                    if (preg_match('#^/gestion/categoria/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'categoria_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\CategoriaController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/gestion/colaboracion')) {
                    // colaboracion
                    if (rtrim($pathinfo, '/') === '/gestion/colaboracion') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'colaboracion');
                        }

                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::indexAction',  '_route' => 'colaboracion',);
                    }

                    // colaboracion_show
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::showAction',));
                    }

                    // colaboracion_concluir
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/concluir$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_concluir')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::concluirAction',));
                    }

                    // colaboracion_parcial
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/parcial$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_parcial')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::parcialAction',));
                    }

                    // colaboracion_hacerparcial
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/hacerparcial$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_hacerparcial')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::hacerparcialAction',));
                    }

                    // colaboracion_new
                    if ($pathinfo === '/gestion/colaboracion/new') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::newAction',  '_route' => 'colaboracion_new',);
                    }

                    // colaboracion_create
                    if ($pathinfo === '/gestion/colaboracion/create') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::createAction',  '_route' => 'colaboracion_create',);
                    }

                    // colaboracion_edit
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::editAction',));
                    }

                    // colaboracion_update
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::updateAction',));
                    }

                    // colaboracion_activar
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/activar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_activar')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::activarAction',));
                    }

                    // colaboracion_delete
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::deleteAction',));
                    }

                    // colaboracion_cancelar
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/cancelar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_cancelar')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::cancelarAction',));
                    }

                    // colaboracion_reportarpago
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/reportarpago$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_reportarpago')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::reportarpagoAction',));
                    }

                    // colaboracion_reportarentrega
                    if (preg_match('#^/gestion/colaboracion/(?P<id>[^/]++)/reportarentrega$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaboracion_reportarentrega')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ColaboracionController::reportarentregaAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/gestion/usuario')) {
                // usuario
                if (rtrim($pathinfo, '/') === '/gestion/usuario') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'usuario');
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::indexAction',  '_route' => 'usuario',);
                }

                // usuario_web
                if ($pathinfo === '/gestion/usuario/usuarioweb') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::usuariowebAction',  '_route' => 'usuario_web',);
                }

                // usuario_desaprobarusuario
                if (preg_match('#^/gestion/usuario/(?P<id>[^/]++)/desaprobarusuario$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_desaprobarusuario')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::desaprobarusuarioAction',));
                }

                // usuario_horas
                if ($pathinfo === '/gestion/usuario/horas') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::horasAction',  '_route' => 'usuario_horas',);
                }

                // usuario_aprobarusuario
                if (preg_match('#^/gestion/usuario/(?P<id>[^/]++)/aprobarusuario$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_aprobarusuario')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::aprobarusuarioAction',));
                }

                // usuario_show
                if (preg_match('#^/gestion/usuario/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::showAction',));
                }

                // usuario_new
                if ($pathinfo === '/gestion/usuario/new') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::newAction',  '_route' => 'usuario_new',);
                }

                // usuario_create
                if ($pathinfo === '/gestion/usuario/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_usuario_create;
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::createAction',  '_route' => 'usuario_create',);
                }
                not_usuario_create:

                // usuario_edit
                if (preg_match('#^/gestion/usuario/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::editAction',));
                }

                // usuario_update
                if (preg_match('#^/gestion/usuario/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_usuario_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::updateAction',));
                }
                not_usuario_update:

                // usuario_delete
                if (preg_match('#^/gestion/usuario/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_usuario_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuario_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\UsuarioController::deleteAction',));
                }
                not_usuario_delete:

            }

            if (0 === strpos($pathinfo, '/gestion/cliente')) {
                // cliente
                if (rtrim($pathinfo, '/') === '/gestion/cliente') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'cliente');
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::indexAction',  '_route' => 'cliente',);
                }

                // cliente_show
                if (preg_match('#^/gestion/cliente/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cliente_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::showAction',));
                }

                // cliente_cc
                if (preg_match('#^/gestion/cliente/(?P<id>[^/]++)/cc$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cliente_cc')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::ccAction',));
                }

                // cliente_imprimir
                if (preg_match('#^/gestion/cliente/(?P<id>[^/]++)/imprimir$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cliente_imprimir')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::imprimirAction',));
                }

                // cliente_new
                if ($pathinfo === '/gestion/cliente/new') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::newAction',  '_route' => 'cliente_new',);
                }

                // cliente_filtro
                if ($pathinfo === '/gestion/cliente/filtro') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::filtroAction',  '_route' => 'cliente_filtro',);
                }

                // cliente_create
                if ($pathinfo === '/gestion/cliente/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_cliente_create;
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::createAction',  '_route' => 'cliente_create',);
                }
                not_cliente_create:

                // cliente_edit
                if (preg_match('#^/gestion/cliente/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cliente_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::editAction',));
                }

                // cliente_update
                if (preg_match('#^/gestion/cliente/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_cliente_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cliente_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::updateAction',));
                }
                not_cliente_update:

                // cliente_delete
                if (preg_match('#^/gestion/cliente/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_cliente_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cliente_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\ClienteController::deleteAction',));
                }
                not_cliente_delete:

            }

            if (0 === strpos($pathinfo, '/gestion/documento')) {
                // documento_gastos
                if ($pathinfo === '/gestion/documento/gastos') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::gastosAction',  '_route' => 'documento_gastos',);
                }

                // documento_show
                if (preg_match('#^/gestion/documento/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'documento_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::showAction',));
                }

                // documento_new
                if (preg_match('#^/gestion/documento/(?P<tipo>[^/]++)/new$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'documento_new')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::newAction',));
                }

                // documento_filtrogasto
                if ($pathinfo === '/gestion/documento/filtrogasto') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::filtrogastoAction',  '_route' => 'documento_filtrogasto',);
                }

                // documento_create
                if ($pathinfo === '/gestion/documento/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_documento_create;
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::createAction',  '_route' => 'documento_create',);
                }
                not_documento_create:

                // documento_edit
                if (preg_match('#^/gestion/documento/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'documento_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::editAction',));
                }

                // documento_borrar
                if (preg_match('#^/gestion/documento/(?P<id>[^/]++)/borrar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'documento_borrar')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::borrarAction',));
                }

                // documento_update
                if (preg_match('#^/gestion/documento/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_documento_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'documento_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::updateAction',));
                }
                not_documento_update:

                // documento_delete
                if (preg_match('#^/gestion/documento/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_documento_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'documento_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::deleteAction',));
                }
                not_documento_delete:

                if (0 === strpos($pathinfo, '/gestion/documento/informe')) {
                    // documento_informeGastos
                    if ($pathinfo === '/gestion/documento/informeGastos') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::informeGastosAction',  '_route' => 'documento_informeGastos',);
                    }

                    // documento_informeVentas
                    if ($pathinfo === '/gestion/documento/informeEntregas') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::informeVentasAction',  '_route' => 'documento_informeVentas',);
                    }

                    // documento_informeDeEntregas
                    if ($pathinfo === '/gestion/documento/informeDeEntregas') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::informeDeEntregasAction',  '_route' => 'documento_informeDeEntregas',);
                    }

                    // documento_informes
                    if ($pathinfo === '/gestion/documento/informes') {
                        return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\DocumentoController::informesAction',  '_route' => 'documento_informes',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/gestion/tipogasto')) {
                // tipogasto
                if (rtrim($pathinfo, '/') === '/gestion/tipogasto') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'tipogasto');
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\TipoGastoController::indexAction',  '_route' => 'tipogasto',);
                }

                // tipogasto_show
                if (preg_match('#^/gestion/tipogasto/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'tipogasto_show')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\TipoGastoController::showAction',));
                }

                // tipogasto_new
                if ($pathinfo === '/gestion/tipogasto/new') {
                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\TipoGastoController::newAction',  '_route' => 'tipogasto_new',);
                }

                // tipogasto_create
                if ($pathinfo === '/gestion/tipogasto/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_tipogasto_create;
                    }

                    return array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\TipoGastoController::createAction',  '_route' => 'tipogasto_create',);
                }
                not_tipogasto_create:

                // tipogasto_edit
                if (preg_match('#^/gestion/tipogasto/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'tipogasto_edit')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\TipoGastoController::editAction',));
                }

                // tipogasto_update
                if (preg_match('#^/gestion/tipogasto/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_tipogasto_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'tipogasto_update')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\TipoGastoController::updateAction',));
                }
                not_tipogasto_update:

                // tipogasto_delete
                if (preg_match('#^/gestion/tipogasto/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_tipogasto_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'tipogasto_delete')), array (  '_controller' => 'GESTION\\GestionBundle\\Controller\\TipoGastoController::deleteAction',));
                }
                not_tipogasto_delete:

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
