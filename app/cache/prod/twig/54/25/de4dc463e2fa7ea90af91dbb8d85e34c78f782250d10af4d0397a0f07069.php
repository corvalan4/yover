<?php

/* GESTIONTiendaBundle:Default:index.html.twig */
class __TwigTemplate_5425de4dc463e2fa7ea90af91dbb8d85e34c78f782250d10af4d0397a0f07069 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONTiendaBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONTiendaBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"container\">
\t\t<section class=\"slider-wrapper fullwidthbanner-container\">
\t\t\t<div class=\"tp-banner-container\">
\t\t\t\t<div class=\"fullwidthbanner\" >
\t\t\t\t\t<ul><!-- SLIDE  -->
\t\t\t\t\t\t<li data-transition=\"fade\" data-slotamount=\"7\" data-masterspeed=\"1500\" >
\t\t\t\t\t\t\t<!-- MAIN IMAGE -->
\t\t\t\t\t\t\t<img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "portada.jpg")), "html", null, true);
        echo "\"  alt=\"slidebg1\"  data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li data-transition=\"fade\" data-slotamount=\"7\" data-masterspeed=\"1500\" >
\t\t\t\t\t\t\t<!-- MAIN IMAGE -->
\t\t\t\t\t\t\t<img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "portada.jpg")), "html", null, true);
        echo "\"  alt=\"slidebg1\"  data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">
\t\t\t\t\t\t</li>
\t\t\t\t\t    <li data-transition=\"fade\" data-slotamount=\"7\" data-masterspeed=\"1500\" >
\t\t\t\t\t\t\t<!-- MAIN IMAGE -->
\t\t\t\t\t\t\t<img src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "portada.jpg")), "html", null, true);
        echo "\"  alt=\"slidebg1\"  data-bgfit=\"cover\" data-bgposition=\"left top\" data-bgrepeat=\"no-repeat\">
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"tp-bannertimer\"></div>
\t\t\t\t</div>
\t\t\t\t<div class=\"slider-shadow\"></div>
\t\t\t</div>
\t\t</section><!-- end slider-wrapper -->
    </div><!-- end container -->
\t<div class=\"messagebox\">
\t\t<h2>BIENVENIDOS A YOVER ®</h2>
\t\t<p class=\"lead\">Lo que es para tí, te encuenta.</p>
\t</div>\t
\t<section class=\"white-wrapper\">
    \t<div class=\"container\">
\t\t\t<div class=\"general-title padding-top\">
\t\t\t\t<h2>Nuestros Productos Destacados</h2>
\t\t\t\t<hr>
\t\t\t\t<p class=\"lead\">Seleccionados especialmente para vos</p>
\t\t\t</div>
\t\t\t<div class=\"shop_wrapper padding-top\">
                <div class=\"padding-top\">
\t\t\t\t\t";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productos"]) ? $context["productos"] : $this->getContext($context, "productos")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["producto"]) {
            // line 43
            echo "\t\t\t\t\t";
            $context["class"] = "";
            // line 44
            echo "\t\t\t\t\t";
            if (twig_in_filter($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), array(0 => 1, 1 => 5, 2 => 9, 3 => 13, 4 => 17, 5 => 21, 6 => 25, 7 => 29, 8 => 33, 9 => 37, 10 => 41, 11 => 45, 12 => 49, 13 => 53, 14 => 57, 15 => 61))) {
                // line 45
                echo "\t\t\t\t\t\t";
                $context["class"] = "first";
                // line 46
                echo "\t\t\t\t\t";
            }
            // line 47
            echo "\t\t\t\t\t";
            if (twig_in_filter($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), array(0 => 4, 1 => 8, 2 => 12, 3 => 16, 4 => 20, 5 => 24, 6 => 28, 7 => 32, 8 => 36, 9 => 40, 10 => 44, 11 => 48, 12 => 52, 13 => 56, 14 => 60, 15 => 64))) {
                // line 48
                echo "\t\t\t\t\t\t";
                $context["class"] = "last";
                // line 49
                echo "\t\t\t\t\t";
            }
            // line 50
            echo "                    <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12 ";
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
            echo "\">
                        <div class=\"shop_item\">
                            <div class=\"entry\">
                                <img src=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "imagenBlob"), "html", null, true);
            echo "\" alt=\"\" class=\"img-responsive\">
                                <div class=\"magnifier\">
                                    <div class=\"buttons\">
                                        <a class=\"st btn btn-default\" rel=\"bookmark\" href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_producto", array("slug" => $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "slug"))), "html", null, true);
            echo "\">Ver</a>
                                    </div><!-- end buttons -->
                                </div><!-- end magnifier -->
                            </div><!-- end entry -->
                            <div class=\"shop_desc\">
                                <div class=\"shop_title pull-left\">
                                    <a href=\"#\"><span>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "nombre"), "html", null, true);
            echo "</span></a>
                                    <span class=\"cats\"><a href=\"";
            // line 63
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_categoria", array("slug" => $this->getAttribute($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "categoria"), "slug"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "categoria"), "nombre"), "html", null, true);
            echo "</a></span>
                                </div>
                                <span class=\"price pull-right\">
\t\t\t\t\t\t\t\t\t";
            // line 66
            if ($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "descuento")) {
                // line 67
                echo "                                    <span style=\"color:red; text-decoration: line-through;\">\$";
                echo twig_escape_filter($this->env, twig_round(($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "precio") * 1.1)), "html", null, true);
                echo "</span>
\t\t\t\t\t\t\t\t\t";
            }
            // line 69
            echo "                                    \$";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "precio"), "html", null, true);
            echo "
                                </span>
                            </div><!-- end shop_desc -->
                        </div><!-- end item -->
                    </div><!-- end col-lg-3 -->
\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['producto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "                </div><!-- end padding-top -->\t\t\t
\t\t\t</div><!-- end shop-wrapper -->
\t\t\t
\t\t\t<div class=\"clearfix\"></div>
\t\t\t
\t\t\t<div class=\"buttons padding-top text-center\">
\t\t\t\t<a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("_tienda");
        echo "\" class=\"btn btn-primary btn-lg\" title=\"\">Ir a la Tienda</a>
\t\t\t</div>
\t\t</div><!-- end container -->
\t</section><!-- end white-wrapper -->
";
    }

    public function getTemplateName()
    {
        return "GESTIONTiendaBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 81,  180 => 75,  159 => 69,  153 => 67,  151 => 66,  143 => 63,  139 => 62,  130 => 56,  124 => 53,  117 => 50,  114 => 49,  111 => 48,  108 => 47,  105 => 46,  102 => 45,  99 => 44,  96 => 43,  79 => 42,  54 => 20,  47 => 16,  40 => 12,  31 => 5,  28 => 2,);
    }
}
