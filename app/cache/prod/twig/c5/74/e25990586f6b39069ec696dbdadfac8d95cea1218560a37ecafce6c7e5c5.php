<?php

/* GESTIONTiendaBundle:Default:novedades.html.twig */
class __TwigTemplate_c574e25990586f6b39069ec696dbdadfac8d95cea1218560a37ecafce6c7e5c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONTiendaBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONTiendaBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<section class=\"post-wrapper-top jt-shadow clearfix\">
\t\t<div class=\"container\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h2>Novedades</h2>
                <ul class=\"breadcrumb pull-right hidden-xs\">
                    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("_index");
        echo "\">Inicio</a></li>
                    <li>Novedades</li>
                </ul>
\t\t\t</div>
\t\t</div>
\t</section><!-- end post-wrapper-top -->

\t<section class=\"blog-wrapper\">
\t\t<div class=\"container\">
            <div class=\"shop_wrapper\">
            \t<div class=\"shop-top\">
                    <div class=\"col-lg-9 col-md-9 col-sm-6 col-xs-12\">
                        <p>Viendo ";
        // line 22
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["productos"]) ? $context["productos"] : $this->getContext($context, "productos"))), "html", null, true);
        echo " resultado</p>
                    </div>
                    <div class=\"pull-right col-lg-3 col-md-3 col-sm-6 col-xs-12\">
                        <select class=\"custom-select form-control\" id=\"orden\">
                            <option value=\"defecto\">Orden por Defecto</option>
                            <option value=\"preciomayormenor\">Precio: Mayor - Menor</option>
                            <option value=\"preciomenormayor\">Precio: Menor - Mayor</option>
                        </select>
                    </div>
\t\t\t\t</div>
                
                <div class=\"clearfix\"></div>
                
                <div class=\"padding-top\">
\t\t\t\t\t";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productos"]) ? $context["productos"] : $this->getContext($context, "productos")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["producto"]) {
            // line 37
            echo "\t\t\t\t\t";
            $context["class"] = "";
            // line 38
            echo "\t\t\t\t\t";
            if (twig_in_filter($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), array(0 => 1, 1 => 5, 2 => 9, 3 => 13, 4 => 17, 5 => 21, 6 => 25, 7 => 29, 8 => 33, 9 => 37, 10 => 41, 11 => 45, 12 => 49, 13 => 53, 14 => 57, 15 => 61))) {
                // line 39
                echo "\t\t\t\t\t\t";
                $context["class"] = "first";
                // line 40
                echo "\t\t\t\t\t";
            }
            // line 41
            echo "\t\t\t\t\t";
            if (twig_in_filter($this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), array(0 => 4, 1 => 8, 2 => 12, 3 => 16, 4 => 20, 5 => 24, 6 => 28, 7 => 32, 8 => 36, 9 => 40, 10 => 44, 11 => 48, 12 => 52, 13 => 56, 14 => 60, 15 => 64))) {
                // line 42
                echo "\t\t\t\t\t\t";
                $context["class"] = "last";
                // line 43
                echo "\t\t\t\t\t";
            }
            // line 44
            echo "                    <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12 ";
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
            echo "\">
                        <div class=\"shop_item\">
                            <div class=\"entry\">
                                <img src=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "imagenBlob"), "html", null, true);
            echo "\" alt=\"\" class=\"img-responsive\">
                                <div class=\"magnifier\">
                                    <div class=\"buttons\">
                                        <a class=\"st btn btn-default\" rel=\"bookmark\" href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_producto", array("slug" => $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "slug"))), "html", null, true);
            echo "\">Ver</a>
                                    </div><!-- end buttons -->
                                </div><!-- end magnifier -->
                            </div><!-- end entry -->
                            <div class=\"shop_desc\">
                                <div class=\"shop_title pull-left\">
                                    <a href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_producto", array("slug" => $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "slug"))), "html", null, true);
            echo "\"><span>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "nombre"), "html", null, true);
            echo "</span></a>
                                    <span class=\"cats\"><a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_categoria", array("slug" => $this->getAttribute($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "categoria"), "slug"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "categoria"), "nombre"), "html", null, true);
            echo "</a></span>
                                </div>
                                <span class=\"price pull-right\">
 \t\t\t\t\t\t\t\t\t";
            // line 60
            if ($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "descuento")) {
                // line 61
                echo "                                    <span style=\"color:red; text-decoration: line-through;\">\$";
                echo twig_escape_filter($this->env, twig_round(($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "precio") * 1.1)), "html", null, true);
                echo "</span>
\t\t\t\t\t\t\t\t\t";
            }
            // line 63
            echo "                                   \$";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "precio"), "html", null, true);
            echo "
                                </span>
                            </div><!-- end shop_desc -->
                        </div><!-- end item -->
                    </div><!-- end col-lg-3 -->
\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['producto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "                </div><!-- end padding-top -->
            </div><!-- end shop-wrapper -->
\t\t</div><!-- end container -->
\t</section><!-- end postwrapper -->

  <script type=\"text/javascript\">
\t\$(document).ready(function() {
\t\t";
        // line 76
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request"), "get", array(0 => "orden"), "method")) {
            // line 77
            echo "\t\t\t\$('#orden').val('";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request"), "get", array(0 => "orden"), "method"), "html", null, true);
            echo "');\t\t
\t\t";
        }
        // line 79
        echo "\t});
\t\$('#orden').change(function(){
\t\twindow.location.href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("_novedades");
        echo "?orden=\"+\$('#orden').val();
\t});
  </script>
";
    }

    public function getTemplateName()
    {
        return "GESTIONTiendaBundle:Default:novedades.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 81,  190 => 79,  184 => 77,  182 => 76,  173 => 69,  152 => 63,  146 => 61,  144 => 60,  136 => 57,  130 => 56,  121 => 50,  115 => 47,  108 => 44,  105 => 43,  102 => 42,  99 => 41,  96 => 40,  93 => 39,  90 => 38,  87 => 37,  70 => 36,  53 => 22,  38 => 10,  31 => 5,  28 => 2,);
    }
}
