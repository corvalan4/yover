<?php

/* GESTIONTiendaBundle:Default:producto.html.twig */
class __TwigTemplate_4d945ef74c8d5796c08046184374d7bc3dc7c2ab8c50b5652083f16bd4ef8ca2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONTiendaBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONTiendaBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<section class=\"post-wrapper-top jt-shadow clearfix\">
\t\t<div class=\"container\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h2>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "nombre"), "html", null, true);
        echo "</h2>
                <ul class=\"breadcrumb pull-right hidden-xs\">
                    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("_index");
        echo "\">Inicio</a></li>
                    <li>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "nombre"), "html", null, true);
        echo "</li>
                </ul>
\t\t\t</div>
\t\t</div>
\t</section>

\t<section class=\"blog-wrapper\">
\t\t<div class=\"container\">
            <div class=\"shop_wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12\">
            \t<div class=\"general_row\">
                    <div class=\"shop-left shop_item col-lg-6\">
                    \t<div class=\"entry\">
\t\t\t\t\t\t\t";
        // line 23
        $context["longimg"] = $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "imagenBlob");
        // line 24
        echo "                        \t<img src=\"";
        echo twig_escape_filter($this->env, (isset($context["longimg"]) ? $context["longimg"] : $this->getContext($context, "longimg")), "html", null, true);
        echo "\" alt=\"\" class=\"img-responsive\">
\t\t\t\t\t\t\t<div class=\"magnifier\">
\t\t\t\t\t\t\t\t<div class=\"buttons\">
\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["longimg"]) ? $context["longimg"] : $this->getContext($context, "longimg")), "html", null, true);
        echo "\" class=\"sf\" title=\"\" data-gal=\"prettyPhoto[product-gallery]\"><span class=\"fa fa-search\"></span></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div><!-- end magnifier -->
                        </div><!-- entry -->
\t\t\t\t\t\t<div class=\"thumbnails clearfix\">
                        \t<div class=\"entry first\">
                            \t<img class=\"img-responsive\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["longimg"]) ? $context["longimg"] : $this->getContext($context, "longimg")), "html", null, true);
        echo "\" alt=\"\" />
                                <div class=\"magnifier\">
                                    <div class=\"buttons\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["longimg"]) ? $context["longimg"] : $this->getContext($context, "longimg")), "html", null, true);
        echo "\" class=\"sf\" title=\"\" data-gal=\"prettyPhoto[product-gallery]\"><span class=\"fa fa-search\"></span></a>
                                    </div><!-- end buttons -->
                                </div><!-- end magnifier -->
                            </div>
\t\t\t\t\t\t\t";
        // line 40
        if ($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "imagendos")) {
            // line 41
            echo "\t\t\t\t\t\t\t";
            $context["longimgdos"] = $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "imagendosBlob");
            // line 42
            echo "                        \t<div class=\"entry\">
                            \t<img class=\"img-responsive\" src=\"";
            // line 43
            echo twig_escape_filter($this->env, (isset($context["longimgdos"]) ? $context["longimgdos"] : $this->getContext($context, "longimgdos")), "html", null, true);
            echo "\" alt=\"\" />
                                <div class=\"magnifier\">
                                    <div class=\"buttons\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 46
            echo twig_escape_filter($this->env, (isset($context["longimgdos"]) ? $context["longimgdos"] : $this->getContext($context, "longimgdos")), "html", null, true);
            echo "\" class=\"sf\" title=\"\" data-gal=\"prettyPhoto[product-gallery]\"><span class=\"fa fa-search\"></span></a>
                                    </div><!-- end buttons -->
                                </div><!-- end magnifier -->
                            </div>
\t\t\t\t\t\t\t";
        }
        // line 51
        echo "\t\t\t\t\t\t\t";
        if ($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "imagentres")) {
            // line 52
            echo "\t\t\t\t\t\t\t";
            $context["longimgtres"] = $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "imagentresBlob");
            // line 53
            echo "                        \t<div class=\"entry last\">
                            \t<img class=\"img-responsive\" src=\"";
            // line 54
            echo twig_escape_filter($this->env, (isset($context["longimgtres"]) ? $context["longimgtres"] : $this->getContext($context, "longimgtres")), "html", null, true);
            echo "\" alt=\"\" />
                                <div class=\"magnifier\">
                                    <div class=\"buttons\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 57
            echo twig_escape_filter($this->env, (isset($context["longimgtres"]) ? $context["longimgtres"] : $this->getContext($context, "longimgtres")), "html", null, true);
            echo "\" class=\"sf\" title=\"\" data-gal=\"prettyPhoto[product-gallery]\"><span class=\"fa fa-search\"></span></a>
                                    </div><!-- end buttons -->
                                </div><!-- end magnifier -->
                            </div>
\t\t\t\t\t\t\t";
        }
        // line 62
        echo "\t\t\t\t\t\t</div>
                    </div><!-- end shop-left -->

                    <div class=\"shop-right col-lg-6\">

                    \t<div class=\"title\">
                        \t<h2>";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "nombre"), "html", null, true);
        echo " </h2>
                            <span class=\"price\">
                            \t\$";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "precio"), "html", null, true);
        echo "
\t\t\t\t\t\t\t</span>
                        </div><!-- end title -->

                        <div class=\"shop_desc\">
                        <p>";
        // line 75
        echo $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "descripcion");
        echo ".</p>
                        </div><!-- end shop_desc -->

                        <div class=\"shop_item_details\">
                        \t<div class=\"title\">
                            \t<h2>Detalle</h2>
                        \t</div><!-- end title -->
                            <ul>
                            \t<li><strong>Código:</strong> ";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "codigo"), "html", null, true);
        echo "</li>
                            \t<li><strong>Categoría:</strong> ";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "categoria"), "nombre"), "html", null, true);
        echo "</li>
                            \t<li><strong style=\"color: green\">Hay Stock!</strong></li>
                            </ul>
                        </div><!-- end shop_item_details -->

                        <div class=\"shop_meta\">
                            <div class=\"pull-left\">
                            \t<div class=\"btn-shop\">
                                    <a href=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_agregar", array("slug" => $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "slug"))), "html", null, true);
        echo "\" class=\"btn woo_btn btn-primary\">Agregar al Carrito</a> 
\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-shopping-cart\"></i>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</a>
                                </div>
                            </div><!-- end pull-right -->
                        </div><!-- end shop meta -->
                    </div><!-- end shop-right -->
                </div><!-- end row -->

                <div class=\"clearfix\"></div>
\t\t\t\t";
        // line 105
        if ((twig_length_filter($this->env, (isset($context["productos"]) ? $context["productos"] : $this->getContext($context, "productos"))) > 0)) {
            // line 106
            echo "                <div class=\"carousel_wrapper\">
                    <div class=\"title\">
                        <h2>Productos Relacionados</h2>
                    </div><!-- end title -->

                    <div class=\"margin-top\">
                        <div id=\"owl_shop_carousel\" class=\"owl-carousel\">
\t\t\t\t\t\t\t";
            // line 113
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["productos"]) ? $context["productos"] : $this->getContext($context, "productos")));
            foreach ($context['_seq'] as $context["_key"] => $context["productorelacionado"]) {
                // line 114
                echo "\t\t\t\t\t\t\t";
                if (((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")) == (isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")))) {
                    // line 115
                    echo "\t\t\t\t\t\t\t\t";
                    $context["longimgrelacion"] = (isset($context["longimg"]) ? $context["longimg"] : $this->getContext($context, "longimg"));
                    // line 116
                    echo "\t\t\t\t\t\t\t";
                } else {
                    // line 117
                    echo "\t\t\t\t\t\t\t\t";
                    $context["longimgrelacion"] = $this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "imagenBlob");
                    // line 118
                    echo "\t\t\t\t\t\t\t";
                }
                // line 119
                echo "\t\t\t\t\t\t\t<div class=\"shop_carousel\">
                                <div class=\"shop_item\">
                                    <div class=\"entry\">
                                        <img src=\"";
                // line 122
                echo twig_escape_filter($this->env, (isset($context["longimgrelacion"]) ? $context["longimgrelacion"] : $this->getContext($context, "longimgrelacion")), "html", null, true);
                echo "\" alt=\"\" class=\"img-responsive\">
                                        <div class=\"magnifier\">
                                            <div class=\"buttons\">
                                                <a class=\"st btn btn-default\" rel=\"bookmark\" href=\"";
                // line 125
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_producto", array("slug" => $this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "slug"))), "html", null, true);
                echo "\">Ver</a>
                                            </div><!-- end buttons -->
                                        </div><!-- end magnifier -->
                                    </div><!-- end entry -->
                                    <div class=\"shop_desc\">
                                        <div class=\"shop_title pull-left\">
                                            <a href=\"";
                // line 131
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_producto", array("slug" => $this->getAttribute((isset($context["producto"]) ? $context["producto"] : $this->getContext($context, "producto")), "slug"))), "html", null, true);
                echo "\"><span>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "nombre"), "html", null, true);
                echo "</span></a>
                                            <span class=\"cats\"><a href=\"";
                // line 132
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_categoria", array("slug" => $this->getAttribute($this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "categoria"), "slug"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "categoria"), "nombre"), "html", null, true);
                echo "</a></span>
                                        </div>
                                        <span class=\"price pull-right\">
\t\t\t\t\t\t\t\t\t\t\t";
                // line 135
                if ($this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "descuento")) {
                    // line 136
                    echo "\t\t\t\t\t\t\t\t\t\t\t<span style=\"color:red; text-decoration: line-through;\">\$";
                    echo twig_escape_filter($this->env, twig_round(($this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "precio") * 1.1)), "html", null, true);
                    echo "</span>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 138
                echo "                                           \$";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["productorelacionado"]) ? $context["productorelacionado"] : $this->getContext($context, "productorelacionado")), "precio"), "html", null, true);
                echo "
                                        </span>
                                    </div><!-- end shop_desc -->
                                </div><!-- end item -->
                            </div><!-- end shop carousel -->
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['productorelacionado'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 144
            echo "                        </div><!-- end owl_blog_three_line -->
                    </div><!-- end padding-top -->
                </div><!-- end carousel_wrapper -->
\t\t\t\t";
        }
        // line 148
        echo "\t\t\t</div><!-- end shop-wrapper -->
\t\t</div><!-- end container -->
\t</section><!-- end postwrapper -->

  <!-- Lightbox Animations -->
  <link href=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "css/prettyPhoto.css")), "html", null, true);
        echo "\" rel=\"stylesheet\">
 <script src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/jquery.prettyPhoto.js")), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t(function(\$) {
\t\t\"use strict\";
\t\tjQuery('a[data-gal]').each(function() {
\t\t\tjQuery(this).attr('rel', jQuery(this).data('gal'));
\t\t});
\t\tjQuery(\"a[data-gal^='prettyPhoto']\").prettyPhoto({animationSpeed:'slow',slideshow:false,overlay_gallery: false,theme:'light_square',social_tools:false,deeplinking:false});
\t})(jQuery);
\t</script>


";
    }

    public function getTemplateName()
    {
        return "GESTIONTiendaBundle:Default:producto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 154,  297 => 153,  290 => 148,  284 => 144,  271 => 138,  265 => 136,  263 => 135,  255 => 132,  249 => 131,  240 => 125,  234 => 122,  229 => 119,  226 => 118,  223 => 117,  220 => 116,  217 => 115,  214 => 114,  210 => 113,  201 => 106,  199 => 105,  183 => 92,  172 => 84,  168 => 83,  157 => 75,  149 => 70,  144 => 68,  136 => 62,  128 => 57,  122 => 54,  119 => 53,  116 => 52,  113 => 51,  105 => 46,  99 => 43,  96 => 42,  93 => 41,  91 => 40,  84 => 36,  78 => 33,  69 => 27,  62 => 24,  60 => 23,  45 => 11,  41 => 10,  36 => 8,  31 => 5,  28 => 2,);
    }
}
