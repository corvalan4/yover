<?php

/* GESTIONGestionBundle:Default:index.html.twig */
class __TwigTemplate_4d497231c8f21aeae168e6591f0c72ca852dfd64abd58cbf44ef9eb7e0cea38c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta name=\"description\" content=\"TITANIO\">
        <meta name=\"keywords\" content=\"TITANIO\">
        <meta name=\"author\" content=\"Corvalan Nicolas\">

        <title>TITANIO</title>

        <!-- DATA TABLE -->
        <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTable/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTable/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
\t\t
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/dataTables.buttons.min.js"), "html", null, true);
        echo "\"> </script> 
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/jszip.min.js"), "html", null, true);
        echo "\"> </script> 
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/pdfmake.min.js"), "html", null, true);
        echo "\"> </script> 
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/vfs_fonts.js"), "html", null, true);
        echo "\"> </script> 
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/buttons.html5.min.js"), "html", null, true);
        echo "\"> </script> 

\t\t<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css'>                     
\t\t<link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTable/jquery.dataTables.css"), "html", null, true);
        echo "\">
        
\t\t<!-- Bootstrap Core CSS -->
        <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/bootstrap/dist/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- MetisMenu CSS -->\t
        <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/metisMenu/dist/metisMenu.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- Custom CSS -->
        <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/dist/css/sb-admin-2.css"), "html", null, true);
        echo "?rand=";
        echo twig_escape_filter($this->env, twig_random($this->env, 5), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- Custom Fonts -->
        <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

        <!-- Bootstrap Core JavaScript -->
        <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/metisMenu/dist/metisMenu.min.js"), "html", null, true);
        echo "\"></script>

        <!-- Custom Theme JavaScript -->
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/dist/js/sb-admin-2.js"), "html", null, true);
        echo "\"></script>
        <!-- Datepicker -->
        <link rel=\"stylesheet\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/datetimepicker/jquery.datetimepicker.css"), "html", null, true);
        echo "\">
        <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/datetimepicker/jquery.datetimepicker.js"), "html", null, true);
        echo "\"></script>\t\t

        <link  href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/joyas/img/favicon.ico"), "html", null, true);
        echo "\" rel=\"icon\" type=\"image/x-icon\" />
        <link rel=\"stylesheet\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/style.css"), "html", null, true);
        echo "?rand=";
        echo twig_escape_filter($this->env, twig_random($this->env, 5), "html", null, true);
        echo "\">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
            <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
\t\t<style>\t
\t\t\tinput, textarea, .uneditable-input {
\t\t\t\twidth: inherit;
\t\t\t}
\t\t</style>
    </head>

    <body>
        <div id=\"content\">
\t\t\t<h1 style=\"text-align: center;\">
\t\t\t\t<img style=\"margin-right: 22px;\" width=\"14%\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/titanio/img/titaniologo.png"), "html", null, true);
        echo "\" />
\t\t\t</h1>
\t\t\t<div class=\"row-fluid\" >
\t\t\t  ";
        // line 72
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 73
            echo "\t\t\t\t  <div class=\"span4 offset4\" >
\t\t\t\t\t<div class=\"alert alert-block fade in alert-error\">
\t\t\t\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t<h4>";
            // line 76
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4> 
\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "\t\t\t  ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgWarn"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 81
            echo "\t\t\t\t  <div class=\"span4 offset4\" >
\t\t\t\t\t<div class=\"alert alert-block fade in alert-warning\">
\t\t\t\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t<h4>";
            // line 84
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "\t\t\t  ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgOk"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 89
            echo "\t\t\t\t  <div class=\"span4 offset4\" >
\t\t\t\t\t<div class=\"alert alert-block fade in alert-success\">
\t\t\t\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t<h4>";
            // line 92
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "\t\t\t  ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgInfo"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 97
            echo "\t\t\t\t  <div class=\"span4 offset4\" >
\t\t\t\t\t<div class=\"alert alert-block fade in alert-info\">
\t\t\t\t\t  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t<h4>";
            // line 100
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "\t\t\t</div>
\t\t\t<div class=\"\">
\t\t\t\t<div class=\"col-md-4 col-md-offset-4\">
\t\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t<h3 class=\"panel-title\">Inicio de Sesi&oacute;n</h3>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t<form action=\"";
        // line 112
        echo $this->env->getExtension('routing')->getPath("_login");
        echo "\" method=\"POST\" autocomplete=\"off\">
\t\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Usuario\" name=\"usuario\" type=\"text\" autofocus=\"\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" placeholder=\"Clave\" name=\"clave\" type=\"password\" value=\"\">
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Ingresar</button>
\t\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"row-fluid\" style=\"margin-top: 30px\">
\t\t\t\t<div class=\"span6 offset3\" style=\"text-align: center; font-size: 100%\">
\t\t\t\t</div>
\t\t\t</div>
        </div>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "GESTIONGestionBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 112,  238 => 104,  228 => 100,  223 => 97,  218 => 96,  208 => 92,  203 => 89,  198 => 88,  188 => 84,  183 => 81,  178 => 80,  168 => 76,  163 => 73,  159 => 72,  153 => 69,  130 => 51,  126 => 50,  121 => 48,  117 => 47,  112 => 45,  106 => 42,  100 => 39,  94 => 36,  86 => 33,  80 => 30,  74 => 27,  68 => 24,  64 => 23,  59 => 21,  55 => 20,  51 => 19,  47 => 18,  43 => 17,  38 => 15,  34 => 14,  19 => 1,);
    }
}
