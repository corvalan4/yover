<?php

/* GESTIONTiendaBundle:Default:nosotros.html.twig */
class __TwigTemplate_2236a2ea7c1682577833ffb070a4ff8385dd48501c82a5f3460d3eabab02b5b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONTiendaBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONTiendaBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<section class=\"post-wrapper-top jt-shadow clearfix\">
\t\t<div class=\"container\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h2>Nosotros</h2>
                <ul class=\"breadcrumb pull-right hidden-xs\">
                    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("_index");
        echo "\">Inicio</a></li>
                    <li>Nosotros</li>
                </ul>
\t\t\t</div>
\t\t</div>
\t</section><!-- end post-wrapper-top -->

\t<section class=\"white-wrapper\">
\t\t<div class=\"container\">
        \t<div class=\"row\">
                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">
                    <div class=\"widget\">
                        <h3>Quienes somos?</h3>
                        <p>
\t\t\t\t\t\t\tSomos un grupo de arquitectos y diseñadores apasionados por el diseño interior. Conformamos YOVER ® como una empresa joven orientada a crear espacios funcionales, cómodos, alegres y propios.
                        </p>
                        <p>
\t\t\t\t\t\t\tNos dedicamos a la importación directa y prestamos un especial cuidado en la calidad de los productos que elegimos: todos y cada uno ofrecen la mejor calidad de terminaciones,  durabilidad y materiales. 
                        </p>
                        <p>
\t\t\t\t\t\t\tContamos con puntos de venta en la Ciudad Autónoma de Buenos Aires, Gran Buenos Aires y hacemos envíos seguros a todo el país.
                        </p>
                        <p>
\t\t\t\t\t\t\tViví YOVER®. Viví tu casa como la querés vivir.
                        </p>
                    </div><!-- end widget -->
                </div><!-- end col-lg-6 -->
            \t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">
                    <div class=\"widget margin-top\">
\t\t\t\t\t\t<img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "../logocuadradosuperior.jpg")), "html", null, true);
        echo "\" class=\"img-responsive\" />
                    </div><!-- end widget -->
\t\t\t\t</div>
\t\t\t</div><!-- end row -->
\t\t</div><!-- end container -->
\t</section><!-- end postwrapper -->
\t
\t<section class=\"grey-wrapper jt-shadow\">
    \t<div class=\"container\">
        \t<div class=\"col-sm-12\">
            \t<div class=\"widget\">
                \t<h3>Nuestros  <span>Pilares</span></h3>
                    <p>
\t\t\t\t\t\tQueremos que nuestros clientes conozcan una experiencia distinta: nuestros profesionales están para asesorarlos con el mismo empeño que utilizarían para decorar su propio espacio.
\t\t\t\t\t\t<br/>
\t\t\t\t\t\tBuscamos que quien llegue a nosotros no solo compre un producto, sino que tenga un apoyo que lo asesore para elegir aquellos elementos que hagan que un lugar se convierta en un HOGAR.
\t\t\t\t\t</p>
                    <div id=\"accordion-first\" class=\"clearfix\">
                        <div class=\"accordion\" id=\"accordion2\">
                          <div class=\"accordion-group\">
                            <div class=\"accordion-heading\">
                              <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#collapseOne\">
                                <em class=\"fa fa-plus icon-fixed-width\"></em>Misión
                              </a>
                            </div>
                            <div id=\"collapseOne\" class=\"accordion-body collapse\">
                              <div class=\"accordion-inner\">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor...
                              </div>
                            </div>
                          </div>
                          <div class=\"accordion-group\">
                            <div class=\"accordion-heading\">
                              <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#collapseTwo\">
                                <em class=\"fa fa-plus icon-fixed-width\"></em>Visión
                              </a>
                            </div>
                            <div id=\"collapseTwo\" class=\"accordion-body collapse\">
                              <div class=\"accordion-inner\">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor...
                              </div>
                            </div>
                          </div>
                          <div class=\"accordion-group\">
                            <div class=\"accordion-heading\">
                              <a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#collapseThree\">
                                <em class=\"fa fa-plus icon-fixed-width\"></em>Valores
                              </a>
                            </div>
                            <div id=\"collapseThree\" class=\"accordion-body collapse\">
                              <div class=\"accordion-inner\">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor...
                              </div>
                            </div>
                          </div>
                        </div><!-- end accordion -->
                    </div><!-- end accordion first -->
                </div><!-- end widget -->
            </div><!-- end col-lg-6 -->
\t\t</div><!-- end container -->
    </section><!-- end grey-wrapper -->
";
    }

    public function getTemplateName()
    {
        return "GESTIONTiendaBundle:Default:nosotros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 39,  38 => 10,  31 => 5,  28 => 2,);
    }
}
