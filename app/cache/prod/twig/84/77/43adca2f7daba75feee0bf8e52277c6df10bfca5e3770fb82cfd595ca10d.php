<?php

/* GESTIONTiendaBundle::base.html.twig */
class __TwigTemplate_847743adca2f7daba75feee0bf8e52277c6df10bfca5e3770fb82cfd595ca10d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>

\t<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">

\t<title>";
        // line 7
        echo twig_escape_filter($this->env, ((array_key_exists("title", $context)) ? (_twig_default_filter((isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "Yover")) : ("Yover")), "html", null, true);
        echo "</title>

\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
\t<meta name=\"description\" content=\"Yover - Indumentaria femenina.\">
\t<meta name=\"keywords\" content=\"Yover, Tienda de Moda, remeras, jeans, calzas, vestidos, moda, accesorios\">
\t<meta name=\"author\" content=\"Nicolas Corvalan\">
\t<link rel=\"icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "../favicon.ico")), "html", null, true);
        echo "\" type=\"image/x-icon\">

\t<!-- Bootstrap Styles -->
\t<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "css/bootstrap.css")), "html", null, true);
        echo "\" rel=\"stylesheet\">

\t<!-- Styles -->
\t<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "style.css")), "html", null, true);
        echo "\" rel=\"stylesheet\">

\t<!-- Carousel Slider -->
\t<link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "css/owl-carousel.css")), "html", null, true);
        echo "\" rel=\"stylesheet\">

\t<!-- CSS Animations -->
\t<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "css/animate.min.css")), "html", null, true);
        echo "\" rel=\"stylesheet\">

\t<!-- Google Fonts -->
\t<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
\t<link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,300italic,700,700italic,900' rel='stylesheet' type='text/css'>
\t<link href='http://fonts.googleapis.com/css?family=Exo:400,300,600,500,400italic,700italic,800,900' rel='stylesheet' type='text/css'>

\t<!-- SLIDER ROYAL CSS SETTINGS -->
\t<link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "royalslider/royalslider.css")), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "royalslider/skins/default-inverted/rs-default-inverted.css")), "html", null, true);
        echo "\" rel=\"stylesheet\">

\t<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "rs-plugin/css/settings.css")), "html", null, true);
        echo "\" media=\"screen\" />

\t<script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/jquery.js")), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/bootstrap.min.js")), "html", null, true);
        echo "\"></script>
\t<!-- Support for HTML5 -->
\t<!--[if lt IE 9]>
\t<script src=\"//html5shim.googlecode.com/svn/trunk/html5.js') }}\"></script>
\t<![endif]-->

\t<!-- Enable media queries on older bgeneral_rowsers -->
\t<!--[if lt IE 9]>
\t<script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/respond.min.js")), "html", null, true);
        echo "\"></script>  <![endif]-->
\t<!-- Global site tag (gtag.js) - Google Analytics -->
\t<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-127571391-2\"></script>
\t<script>
\t\twindow.dataLayer = window.dataLayer || [];
\t\tfunction gtag(){dataLayer.push(arguments);}
\t\tgtag('js', new Date());

\t\tgtag('config', 'UA-127571391-2');
\t</script>

</head>
<body>
";
        // line 61
        $context["cliente"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "cliente"), "method");
        // line 62
        echo "<div class=\"animationload\">
\t<div class=\"loader\">Loading...</div>
</div>
\t<div id=\"topbar\" class=\"clearfix\">
    \t<div class=\"container\">
            <div class=\"col-lg-4 col-md-4 col-sm-4\">
                <div class=\"social-icons\">
                    <span><a data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Facebook\" href=\"https://www.facebook.com/yoverok/\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></span>
                    <span class=\"last\"><a data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Instagram\" href=\"https://www.instagram.com/yoverok/\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></span>
                </div><!-- end social icons -->
            </div><!-- end columns -->
            <div class=\"col-lg-8 col-md-8 col-sm-8\">
                <div class=\"topmenu\">
                    ";
        // line 75
        if (twig_test_empty((isset($context["cliente"]) ? $context["cliente"] : $this->getContext($context, "cliente")))) {
            // line 76
            echo "                        <span class=\"topbar-login\"><i class=\"fa fa-user\"></i> <a href=\"";
            echo $this->env->getExtension('routing')->getPath("_ingresar");
            echo "\">Ingresar</a></span>
                    ";
        } else {
            // line 78
            echo "                        <span class=\"topbar-login\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_mispedidos");
            echo "\"><i class=\"fa fa-user\"></i> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cliente"]) ? $context["cliente"] : $this->getContext($context, "cliente")), "razonSocial"), "html", null, true);
            echo "</a></span>
                    ";
        }
        // line 80
        echo "\t\t\t\t\t";
        $context["carrito"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "carrito"), "method");
        // line 81
        echo "                    <span class=\"topbar-cart\"><i class=\"fa fa-shopping-cart\"></i> <a href=\"";
        echo $this->env->getExtension('routing')->getPath("_checkout");
        echo "\">";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["carrito"]) ? $context["carrito"] : $this->getContext($context, "carrito"))), "html", null, true);
        echo " item</a></span>
                </div><!-- end top menu -->
            </div><!-- end columns -->
        </div><!-- end container -->
    </div><!-- end topbar -->

    <header id=\"header-style-1\">
\t\t<div class=\"container\">
\t\t\t<nav class=\"navbar yamm navbar-default\">
\t\t\t\t<div class=\"navbar-header\">
                    <button type=\"button\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-1\" class=\"navbar-toggle\">
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a href=\"";
        // line 96
        echo $this->env->getExtension('routing')->getPath("_index");
        echo "\" class=\"navbar-brand\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "logo.png")), "html", null, true);
        echo "\"/></a>
        \t\t</div><!-- end navbar-header -->

\t\t\t\t<div id=\"navbar-collapse-1\" class=\"navbar-collapse collapse navbar-right\">
\t\t\t\t\t<ul class=\"nav navbar-nav\">
\t\t\t\t\t\t<li><a href=\"";
        // line 101
        echo $this->env->getExtension('routing')->getPath("_index");
        echo "\">Inicio </a></li><!-- end drop down -->
                        <li><a href=\"";
        // line 102
        echo $this->env->getExtension('routing')->getPath("_nosotros");
        echo "\">Nosotros</a></li>
                        <li><a href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath("_novedades");
        echo "\">Novedades</a></li>
                        <li><a href=\"";
        // line 104
        echo $this->env->getExtension('routing')->getPath("_tienda");
        echo "\">Tienda</a></li>
                        <li><a href=\"";
        // line 105
        echo $this->env->getExtension('routing')->getPath("_contacto");
        echo "\">Contacto</a></li>
\t\t\t\t\t</ul><!-- end navbar-nav -->
\t\t\t\t</div><!-- #navbar-collapse-1 -->
\t\t\t</nav><!-- end navbar yamm navbar-default -->
\t\t</div><!-- end container -->
\t</header><!-- end header-style-1 -->
    <div class=\"row\" >
        ";
        // line 112
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 113
            echo "            <div class=\"col-md-6 col-md-offset-3\" >
                <div class=\"alert alert-danger fade in\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" onclick=\"\$('.alert').hide()\">X</button>
                    <h4>";
            // line 116
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgWarn"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 121
            echo "            <div class=\"col-md-6 col-md-offset-3\" >
                <div class=\"alert alert-info fade in\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" onclick=\"\$('.alert').hide()\">X</button>
                    <h4>";
            // line 124
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 128
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgOk"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 129
            echo "            <div class=\"col-md-6 col-md-offset-3\" >
                <div class=\"alert alert-success fade in\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" onclick=\"\$('.alert').hide()\">X</button>
                    <h4>";
            // line 132
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 136
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgInfo"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 137
            echo "            <div class=\"col-md-6 col-md-offset-3\" >
                <div class=\"alert alert-info fade in\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" onclick=\"\$('.alert').hide()\">X</button>
                    <h4>";
            // line 140
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 144
        echo "    </div>

\t";
        // line 146
        $this->displayBlock('body', $context, $blocks);
        // line 148
        echo "
\t<footer id=\"footer-style-1\">
    \t<div class=\"container\">
        \t<div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12\">
            \t<div class=\"widget\">
                \t<div class=\"title\">
                        <h3>Categorías</h3>
                    </div><!-- end title -->
                    <ul class=\"footer_post\" style=\"display: grid;\">
\t\t\t\t\t";
        // line 157
        $context["categorias"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "categorias"), "method");
        // line 158
        echo "\t\t\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categorias"]) ? $context["categorias"] : $this->getContext($context, "categorias")));
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            // line 159
            echo "                        <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_categoria", array("slug" => $this->getAttribute((isset($context["categoria"]) ? $context["categoria"] : $this->getContext($context, "categoria")), "slug"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["categoria"]) ? $context["categoria"] : $this->getContext($context, "categoria")), "nombre"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 161
        echo "\t\t\t\t\t</ul><!-- end twiteer_feed -->
                </div><!-- end widget -->
            </div><!-- end columns -->
        \t<div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12\">
            \t<div class=\"widget\">
                \t<div class=\"title\">
                        <h3>Mi Cuenta</h3>
                    </div><!-- end title -->
                    <ul class=\"footer_post\" style=\"display: grid;\">
                        ";
        // line 170
        if (twig_test_empty((isset($context["cliente"]) ? $context["cliente"] : $this->getContext($context, "cliente")))) {
            // line 171
            echo "                            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_ingresar");
            echo "\">Ingresar</a></li>
                        ";
        } else {
            // line 173
            echo "                            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_mispedidos");
            echo "\">Mis Pedidos</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 174
            echo $this->env->getExtension('routing')->getPath("_misdatos");
            echo "\">Mi Datos Personales</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 175
            echo $this->env->getExtension('routing')->getPath("_salir");
            echo "\">Cerrar Sesión</a></li>
                        ";
        }
        // line 177
        echo "\t\t\t\t\t</ul><!-- end twiteer_feed -->
                </div><!-- end widget -->
            </div><!-- end columns -->
        \t<div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-12\">
            \t<div class=\"widget\">
                \t<div class=\"title\">
                        <h3>Contactanos</h3>
                    </div><!-- end title -->
                    <ul class=\"footer_post\">
                        <li>info@yover.com.ar</li>
                        <li>+54 11 5229 2925</li>
                    </ul><!-- recent posts -->
                </div><!-- end widget -->
            </div><!-- end columns -->
    \t</div><!-- end container -->
    </footer><!-- end #footer-style-1 -->

\t<div id=\"copyrights\">
    \t<div class=\"container\">
\t\t\t<div class=\"col-lg-5 col-md-6 col-sm-12\">
            \t<div class=\"copyright-text\">
                    <p>Copyright © 2018 - Desarrollo <a href=\"http://sistemastitanio.com.ar\" target=\"_blank\"><strong>Sistemas TITANIO</strong></a></p>
                </div><!-- end copyright-text -->
\t\t\t</div><!-- end widget -->
\t\t\t<div class=\"col-lg-7 col-md-6 col-sm-12 clearfix\">
\t\t\t\t<div class=\"footer-menu\">
                    <ul class=\"menu\">
                        <li class=\"active\"><a href=\"#\">Inicio</a></li>
                        <li><a href=\"";
        // line 205
        echo $this->env->getExtension('routing')->getPath("_nosotros");
        echo "\">Nosotros</a></li>
                        <li><a href=\"";
        // line 206
        echo $this->env->getExtension('routing')->getPath("_novedades");
        echo "\">Novedades</a></li>
                        <li><a href=\"";
        // line 207
        echo $this->env->getExtension('routing')->getPath("_tienda");
        echo "\">Tienda</a></li>
                        <li><a href=\"";
        // line 208
        echo $this->env->getExtension('routing')->getPath("_contacto");
        echo "\">Contacto</a></li>
                    </ul>
                </div>
\t\t\t</div><!-- end large-7 -->
        </div><!-- end container -->
    </div><!-- end copyrights -->

\t<div class=\"dmtop\">Scroll Top</div>

  <!-- Main Scripts-->
  <script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/menu.js")), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/owl.carousel.min.js")), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/jquery.parallax-1.1.3.js")), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/jquery.simple-text-rotator.js")), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/wow.min.js")), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/custom.js")), "html", null, true);
        echo "\"></script>

  <script src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/jquery.isotope.min.js")), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "js/custom-portfolio.js")), "html", null, true);
        echo "\"></script>

  <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
  <script type=\"text/javascript\" src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "rs-plugin/js/jquery.themepunch.plugins.min.js")), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "rs-plugin/js/jquery.themepunch.revolution.min.js")), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\">
\t\tjQuery(document).ready(function() {
\t\t\tjQuery('.fullwidthbanner').revolution(
\t\t\t\t{
\t\t\t\tdelay:6000,
\t\t\t\tstartwidth:960,
\t\t\t\tstartheight:426,
\t\t\t\thideThumbs:10
\t\t\t\t}
\t\t\t);
\t\t});
  </script>


  <!-- Royal Slider script files -->
  <script src=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "royalslider/jquery.easing-1.3.js")), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 247
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((isset($context["pluginsurl"]) ? $context["pluginsurl"] : $this->getContext($context, "pluginsurl")) . "royalslider/jquery.royalslider.min.js")), "html", null, true);
        echo "\"></script>
  <script>
\tjQuery(document).ready(function(\$) {
\t  var rsi = \$('#slider-in-laptop').royalSlider({
\t\tautoHeight: false,
\t\tarrowsNav: false,
\t\tfadeinLoadedSlide: false,
\t\tcontrolNavigationSpacing: 0,
\t\tcontrolNavigation: 'bullets',
\t\timageScaleMode: 'fill',
\t\timageAlignCenter: true,
\t\tloop: false,
\t\tloopRewind: false,
\t\tnumImagesToPreload: 6,
\t\tkeyboardNavEnabled: true,
\t\tautoScaleSlider: true,
\t\tautoScaleSliderWidth: 486,
\t\tautoScaleSliderHeight: 315,
\t\timgWidth: 792,
\t\timgHeight: 479

\t  }).data('royalSlider');
\t  \$('#slider-next').click(function() {
\t\trsi.next();
\t  });
\t  \$('#slider-prev').click(function() {
\t\trsi.prev();
\t  });
\t});
  </script>
  <style>
  .close{
\t  line-height: 0;
  }
  </style>
  </body>
</html>
";
    }

    // line 146
    public function block_body($context, array $blocks = array())
    {
        // line 147
        echo "\t";
    }

    public function getTemplateName()
    {
        return "GESTIONTiendaBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  520 => 147,  517 => 146,  475 => 247,  471 => 246,  452 => 230,  448 => 229,  442 => 226,  438 => 225,  433 => 223,  429 => 222,  425 => 221,  421 => 220,  417 => 219,  413 => 218,  400 => 208,  396 => 207,  392 => 206,  388 => 205,  358 => 177,  353 => 175,  349 => 174,  344 => 173,  338 => 171,  336 => 170,  325 => 161,  314 => 159,  309 => 158,  307 => 157,  296 => 148,  294 => 146,  290 => 144,  280 => 140,  275 => 137,  270 => 136,  260 => 132,  255 => 129,  250 => 128,  240 => 124,  235 => 121,  230 => 120,  220 => 116,  215 => 113,  211 => 112,  201 => 105,  197 => 104,  193 => 103,  189 => 102,  185 => 101,  175 => 96,  154 => 81,  151 => 80,  143 => 78,  137 => 76,  135 => 75,  120 => 62,  118 => 61,  102 => 48,  91 => 40,  87 => 39,  82 => 37,  76 => 34,  72 => 33,  61 => 25,  55 => 22,  49 => 19,  43 => 16,  37 => 13,  28 => 7,  20 => 1,);
    }
}
