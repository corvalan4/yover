<?php

/* GESTIONGestionBundle:Categoria:index.html.twig */
class __TwigTemplate_958d9b3d439027379ec6fc1bcd20a565932fe039c8863e67cd52ea1dc341eacb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONGestionBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONGestionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>
\t\tListado de Categorias
\t\t<a class=\"btn btn-primary\" style=\"float:right;\" href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("categoria_new");
        echo "\">
\t\t\tCrear
\t\t</a>
\t</h1>
\t<div class=\"gestion\">
\t\t<table class=\"display\" id=\"tablas\" width=\"100%\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>ID</th>
\t\t\t\t\t<th>Nombre</th>
\t\t\t\t\t<th>Slug</th>
\t\t\t\t\t<th>Acciones</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 22
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "slug"), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>
\t\t\t\t\t";
            // line 27
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "codEstado") == "A")) {
                // line 28
                echo "\t\t\t\t\t\t<a class=\"btn btn-success btn-xs\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categoria_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Editar\"><i class=\"fa fa-pencil\"></i></a>
\t\t\t\t\t\t<a class=\"btn btn-danger btn-xs\" href=\"";
                // line 29
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categoria_delete", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Borrar\"><i class=\"fa fa-times\"></i></a>
\t\t\t\t\t";
            } else {
                // line 31
                echo "\t\t\t\t\t\t<a class=\"btn btn-primary btn-xs\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categoria_activar", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Activar\">Activar</a>
\t\t\t\t\t";
            }
            // line 33
            echo "\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "\t\t\t</tbody>
\t\t</table>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "GESTIONGestionBundle:Categoria:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 36,  91 => 33,  85 => 31,  80 => 29,  75 => 28,  73 => 27,  68 => 25,  64 => 24,  60 => 23,  57 => 22,  53 => 21,  35 => 6,  31 => 4,  28 => 3,);
    }
}
