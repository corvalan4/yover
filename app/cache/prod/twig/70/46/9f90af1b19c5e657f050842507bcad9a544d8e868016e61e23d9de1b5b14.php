<?php

/* GESTIONGestionBundle:Elemento:new.html.twig */
class __TwigTemplate_70469f90af1b19c5e657f050842507bcad9a544d8e868016e61e23d9de1b5b14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONGestionBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONGestionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>
\t\tCrear Producto
\t\t<a class=\"btn btn-default\" style=\"float:right;\" href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("elemento");
        echo "\">
\t\t\tVolver
\t\t</a>
\t</h1>
\t<div class=\"col-md-6 col-md-offset-3 gestion\">
\t\t";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
\t\t
\t\t";
        // line 13
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
\t</div>
\t<script>
\t\t\$(document).ready(function(){
\t\t\t\$('#gestion_gestionbundle_elemento_destacado,#gestion_gestionbundle_elemento_novedad').removeAttr('required');
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "GESTIONGestionBundle:Elemento:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 13,  43 => 11,  35 => 6,  31 => 4,  28 => 3,);
    }
}
