<?php

/* GESTIONGestionBundle:Default:inicio.html.twig */
class __TwigTemplate_f7e73039f6042a5edb3e99ad866d6eef3fc78030a51e4ee9fae6e6d62cbb5681 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONGestionBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONGestionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 4
        $context["desde"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "desde"), "method");
        // line 5
        $context["fin"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "fin"), "method");
        // line 6
        echo "
<div class=\"col-md-12 text-center\">
\t<img style=\"margin-right: 22px;\" width=\"20%\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/titanio/img/titaniologo.png"), "html", null, true);
        echo "\" />
</div>
<div class=\"col-md-12 text-center\">
\t<h2>
\t\tBienvenido al Sistema de Gestión
\t</h2>
</div>
<div class=\"col-md-12 text-center\">
\t<h3>
\t\tAccesos Rápidos
\t</h3>
\t<div class=\"col-md-3 text-center\">
\t\t<a class=\"btn btn-success btn-lg\" href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("colaboracion");
        echo "\">Entregas <i class=\"fa fa-cubes\"></i></a>
\t</div>
\t<div class=\"col-md-3 text-center\">
\t\t<a class=\"btn btn-info btn-lg\" href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("cliente");
        echo "\">Clientes <i class=\"fa fa-users\"></i></a>
\t</div>
\t<div class=\"col-md-3 text-center\">
\t\t<a class=\"btn btn-danger btn-lg\" href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("documento_gastos");
        echo "\">Gastos <i class=\"fa fa-level-down\"></i></a>
\t</div>
\t<div class=\"col-md-3 text-center\">
\t\t<a class=\"btn btn-primary btn-lg\" href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("_ganancias");
        echo "\">Informe Ganancia Neta <i class=\"fa fa-dollar\"></i></a>
\t</div>
</div>
<script>
\tvar fin = '";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["fin"]) ? $context["fin"] : $this->getContext($context, "fin")), "html", null, true);
        echo "';\t\t
\tif(fin=='no'){
\t\tvar direccion = '";
        // line 35
        echo $this->env->getExtension('routing')->getPath("_importarArchivos");
        echo "?desde=";
        echo twig_escape_filter($this->env, (isset($context["desde"]) ? $context["desde"] : $this->getContext($context, "desde")), "html", null, true);
        echo "';
\t\twindow.location.href = direccion;
\t}
</script>
";
    }

    public function getTemplateName()
    {
        return "GESTIONGestionBundle:Default:inicio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 35,  79 => 33,  72 => 29,  66 => 26,  60 => 23,  54 => 20,  39 => 8,  35 => 6,  33 => 5,  31 => 4,  28 => 2,);
    }
}
