<?php

/* GESTIONGestionBundle:Elemento:index.html.twig */
class __TwigTemplate_cd443929b1aa3d9c9e3f223cd4d2a72d64bff800879098463820b40af49a79ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GESTIONGestionBundle::base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GESTIONGestionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>
\t\tListado de Productos
\t\t<a class=\"btn btn-primary\" style=\"float:right;\" href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("elemento_new");
        echo "\">
\t\t\tCrear
\t\t</a>
\t</h1>
\t<div class=\"gestion\">
\t\t<table class=\"display\" id=\"tablas\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th>ID</th>
\t\t\t\t\t<th>Nombre</th>
\t\t\t\t\t<th>Categoría</th>
\t\t\t\t\t<th>Stock</th>
\t\t\t\t\t<th>Precio</th>
\t\t\t\t\t<th>Acciones</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 24
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "codigo"), "html", null, true);
            echo "</td>
\t\t\t\t\t<td><a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elemento_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
            echo "</a></td>
\t\t\t\t\t<td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "categoria"), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "stock"), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>\$";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "precio"), "html", null, true);
            echo "</td>
\t\t\t\t\t<td>
\t\t\t\t\t";
            // line 31
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "codEstado") == "A")) {
                // line 32
                echo "\t\t\t\t\t\t<a class=\"btn btn-info btn-xs\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elementostock", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Manejo de Stock\"><i class=\"fa fa-plus\"></i></a>
\t\t\t\t\t\t";
                // line 33
                if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "visible")) {
                    // line 34
                    echo "\t\t\t\t\t\t<a class=\"btn btn-default btn-xs\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elemento_novisible", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                    echo "\" title=\"No Visible\"><i class=\"fa fa-eye-slash\"></i></a>
\t\t\t\t\t\t";
                } else {
                    // line 36
                    echo "\t\t\t\t\t\t<a class=\"btn btn-default btn-xs\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elemento_visible", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                    echo "\" title=\"Hacer Visible\"><i class=\"fa fa-eye\"></i></a>
\t\t\t\t\t\t";
                }
                // line 38
                echo "\t\t\t\t\t\t<a class=\"btn btn-success btn-xs\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elemento_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Editar\"><i class=\"fa fa-pencil\"></i></a>
\t\t\t\t\t\t<a class=\"btn btn-primary btn-xs\" href=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elemento_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Ver\"><i class=\"fa fa-search\"></i></a>
\t\t\t\t\t\t<a class=\"btn btn-danger btn-xs\" href=\"";
                // line 40
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elemento_delete", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Borrar\"><i class=\"fa fa-times\"></i></a>
\t\t\t\t\t";
            } else {
                // line 42
                echo "\t\t\t\t\t\t<a class=\"btn btn-primary btn-xs\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("elemento_activar", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" title=\"Activar\">Activar</a>
\t\t\t\t\t";
            }
            // line 44
            echo "\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "\t\t\t</tbody>
\t\t</table>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "GESTIONGestionBundle:Elemento:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 47,  126 => 44,  120 => 42,  115 => 40,  111 => 39,  106 => 38,  100 => 36,  94 => 34,  92 => 33,  87 => 32,  85 => 31,  80 => 29,  76 => 28,  72 => 27,  66 => 26,  62 => 25,  59 => 24,  55 => 23,  35 => 6,  31 => 4,  28 => 3,);
    }
}
