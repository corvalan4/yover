<?php

/* GESTIONGestionBundle::base.html.twig */
class __TwigTemplate_9431fa1913d3deb939c8e14c5a82ca1566e840a38826b22e718af8322e3b6d6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta name=\"description\" content=\"TITANIO\">
        <meta name=\"keywords\" content=\"TITANIO\">
        <meta name=\"author\" content=\"Corvalan Nicolas\">

        <title>TITANIO</title>

        <!-- DATA TABLE -->
        <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTable/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTable/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>

\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/dataTables.buttons.min.js"), "html", null, true);
        echo "\"> </script>
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/jszip.min.js"), "html", null, true);
        echo "\"> </script>
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/pdfmake.min.js"), "html", null, true);
        echo "\"> </script>
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/vfs_fonts.js"), "html", null, true);
        echo "\"> </script>
\t\t<script language=\"JavaScript\" type=\"text/JavaScript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/buttons.html5.min.js"), "html", null, true);
        echo "\"> </script>

\t\t<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTableEXPORT/buttons.dataTables.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css'>
\t\t<link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/DataTable/jquery.dataTables.css"), "html", null, true);
        echo "\">

\t\t<!-- Bootstrap Core CSS -->
        <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/bootstrap/dist/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- MetisMenu CSS -->
        <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/metisMenu/dist/metisMenu.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- Custom CSS -->
        <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/dist/css/sb-admin-2.css"), "html", null, true);
        echo "?rand=";
        echo twig_escape_filter($this->env, twig_random($this->env, 5), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- Custom Fonts -->
        <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

        <!-- Bootstrap Core JavaScript -->
        <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/bower_components/metisMenu/dist/metisMenu.min.js"), "html", null, true);
        echo "\"></script>

        <!-- Custom Theme JavaScript -->
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/nuevo/dist/js/sb-admin-2.js"), "html", null, true);
        echo "\"></script>
        <!-- Datepicker -->
        <link rel=\"stylesheet\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/datetimepicker/jquery.datetimepicker.css"), "html", null, true);
        echo "\">
        <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/datetimepicker/jquery.datetimepicker.js"), "html", null, true);
        echo "\"></script>

        <link  href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/joyas/img/favicon.ico"), "html", null, true);
        echo "\" rel=\"icon\" type=\"image/x-icon\" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
            <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <script>
            var table;
            var tableSF;
\t\t\tfunction borrar(link){
\t\t\t\tif(confirm(\"Esta seguro que desea borrar?\")){
\t\t\t\t\twindow.location.href=link;
\t\t\t\t}
\t\t\t}

            \$(document).ready(function () {
                \$('h1>a').css('float', 'right');
                \$('input').addClass('form-control');

\t\t\t\t\$( \"input:file\" ).removeClass('form-control').removeAttr('required');
\t\t\t\t\$('input:first').focus();

                table = \$('#tablas').DataTable({
                    \"scrollY\": \"600px\",
                    \"scrollX\": true,
                    \"scrollCollapse\": true,
                    \"paging\": false,
                    \"info\": false,
                    responsive: true,
                    \"filter\": true,
                    \"order\": [[0, \"asc\"]],
                    \"oLanguage\": {
\t\t\t\t\t\t\"sSearch\": \t\t   \"Buscar:\",
\t\t\t\t\t\t\"sEmptyTable\": \t   \"No se encontraron registros.\",
\t\t\t\t\t\t\"sProcessing\":     \"Procesando...\",
\t\t\t\t\t\t\"sLengthMenu\":     \"Mostrar _MENU_ registros\",
\t\t\t\t\t\t\"sZeroRecords\":    \"No se encontraron resultados\",
\t\t\t\t\t\t\"sInfo\":           \"Mostrando registros del: _START_ al _END_ (de un total de _TOTAL_ registros)\",
\t\t\t\t\t\t\"sInfoEmpty\":      \"Mostrando registros del 0 al 0 de un total de 0 registros\",
\t\t\t\t\t\t\"sInfoFiltered\":   \"(filtrado de un total de _MAX_ registros)\",
\t\t\t\t\t\t\"sInfoPostFix\":    \"\",
\t\t\t\t\t\t\"sSearch\":         \"Buscar:\",
\t\t\t\t\t\t\"sUrl\":            \"\",
\t\t\t\t\t\t\"sInfoThousands\":  \",\",
\t\t\t\t\t\t\"sLoadingRecords\": \"Cargando...\"
\t\t\t\t\t}
                });

                tableSF = \$('#tablaSF').DataTable({
                    \"scrollY\": \"400px\",
                    \"scrollCollapse\": true,
                    \"scrollX\": true,
                    \"paging\": false,
                    \"info\": false,
                    responsive: true,
                    \"filter\": false,
                    \"order\": [[0, \"desc\"]]
                });

                \$(\".datetimepicker\").datetimepicker({
                    format: 'Y-m-d'
                });

                \$(\"select\").addClass('form-control');
                \$('[data-toggle=\"tooltip\"]').tooltip();

            });

            \$(window).resize(function () {
                \$('#tablas').DataTable().columns.adjust().draw();
                \$('#tablaSF').DataTable().columns.adjust().draw();
            });
            \$(window).load(function () {
                \$('.preloader').fadeOut(1000); // set duration in brackets
            });
        </script>
        <link rel=\"stylesheet\" href=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/style.css"), "html", null, true);
        echo "?rand=";
        echo twig_escape_filter($this->env, twig_random($this->env, 5), "html", null, true);
        echo "\">
    </head>

\t";
        // line 131
        $context["perfil"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "perfil"), "method");
        // line 132
        echo "\t";
        $context["idUsuario"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "idUsuario"), "method");
        // line 133
        echo "
    <body>
        <div id=\"wrapper\">
            <!-- Navigation -->
            <nav class=\"navbar navbar-default navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"";
        // line 145
        echo $this->env->getExtension('routing')->getPath("_inicio");
        echo "\" >
\t\t\t\t\t\t<img style=\"width: 120px;\" src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/titanio/img/titaniologo.png"), "html", null, true);
        echo "\" />
                    </a>
                </div>
\t\t\t\t<ul class=\"nav navbar-top-links navbar-right\">
\t\t\t\t\t<!-- /.dropdown -->
\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
\t\t\t\t\t\t  ";
        // line 153
        $context["usuario"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "get", array(0 => "usuario"), "method");
        // line 154
        echo "\t\t\t\t\t\t  ";
        echo twig_escape_filter($this->env, (isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "html", null, true);
        echo " <i class=\"fa fa-user fa-fw\"></i>  <i class=\"fa fa-caret-down\"></i>
\t\t\t\t\t\t</a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-user\">
\t\t\t\t\t\t\t<li><a href=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("usuario_edit", array("id" => (isset($context["idUsuario"]) ? $context["idUsuario"] : $this->getContext($context, "idUsuario")))), "html", null, true);
        echo "\"><i class=\"fa fa-gear fa-fw\"></i>Mis Datos</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li class=\"divider\"></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 160
        echo $this->env->getExtension('routing')->getPath("_cerrar");
        echo "\"><i class=\"fa fa-sign-out fa-fw\"></i> Cerrar Sesi&oacute;n</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</li>
\t\t\t\t</ul>
                <div class=\"navbar-default sidebar\" role=\"navigation\">
                    <div class=\"sidebar-nav navbar-collapse\">
                        <ul class=\"nav\" id=\"side-menu\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-list fa-fw\"></i> Gestión<span class=\"fa arrow\"></span></a>
\t\t\t\t\t\t\t\t<ul class=\"nav nav-second-level\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 172
        echo $this->env->getExtension('routing')->getPath("elemento");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tListado de Productos
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 177
        echo $this->env->getExtension('routing')->getPath("colaboracion");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tEntrega de Productos
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 182
        echo $this->env->getExtension('routing')->getPath("documento_gastos");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tGastos
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 189
        echo $this->env->getExtension('routing')->getPath("cliente");
        echo "\"><i class=\"fa fa-users\"></i> Clientes</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        // line 191
        if (((isset($context["perfil"]) ? $context["perfil"] : $this->getContext($context, "perfil")) == "ADMINISTRADOR")) {
            // line 192
            echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-file-text fa-fw\"></i> Cargas Masivas<span class=\"fa arrow\"></span></a>
\t\t\t\t\t\t\t\t<ul class=\"nav nav-second-level\">
\t\t\t\t\t\t\t\t\t<li id=\"cargamasiva\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 196
            echo $this->env->getExtension('routing')->getPath("_cargamasiva");
            echo "\">
\t\t\t\t\t\t\t\t\t\t\tCarga Masiva de Productos
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li id=\"cargafacturas\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 201
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/templateImport.xlsx"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\tDescarga excel p/importaci&oacute;n
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 208
        echo " \t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-file-excel-o fa-fw\"></i> Informes<span class=\"fa arrow\"></span></a>
\t\t\t\t\t\t\t\t<ul class=\"nav nav-second-level\">
\t\t\t\t\t\t\t\t";
        // line 211
        if (((isset($context["perfil"]) ? $context["perfil"] : $this->getContext($context, "perfil")) == "ADMINISTRADOR")) {
            // line 212
            echo "\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 213
            echo $this->env->getExtension('routing')->getPath("_ganancias");
            echo "\">
\t\t\t\t\t\t\t\t\t\t\tGanancia (Precio - Costo)
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 218
            echo $this->env->getExtension('routing')->getPath("_gananciaventas");
            echo "\">
\t\t\t\t\t\t\t\t\t\t\tGanancia Neta (Ventas - Gastos)
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t";
        }
        // line 223
        echo "\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 224
        echo $this->env->getExtension('routing')->getPath("documento_informeGastos");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tInforme Estad&iacute;stico de Gastos
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 229
        echo $this->env->getExtension('routing')->getPath("documento_informeVentas");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tInforme Estad&iacute;stico de Entregas
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 234
        echo $this->env->getExtension('routing')->getPath("elemento_listado");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tListado p/control
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 239
        echo $this->env->getExtension('routing')->getPath("elemento_listadocategoria");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tListado p/control por Categoría
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 244
        echo $this->env->getExtension('routing')->getPath("informeingresosegresos");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tListado de Ingresos y Egresos
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
 \t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-cog fa-fw\"></i> Parametría<span class=\"fa arrow\"></span></a>
\t\t\t\t\t\t\t\t<ul class=\"nav nav-second-level\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 254
        echo $this->env->getExtension('routing')->getPath("categoria");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tCategorías
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 259
        echo $this->env->getExtension('routing')->getPath("tipogasto");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tTipo de Gastos
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 264
        echo $this->env->getExtension('routing')->getPath("usuario");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\tUsuarios
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
\t\t\t\t<div id=\"page-wrapper\">
                    <div class=\"container-fluid\">
                        <div class=\"row-fluid\" >
                            ";
        // line 281
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 282
            echo "                                <div class=\"span4 offset4\" >
                                    <div class=\"alert alert-block fade in alert-error\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                        <h4>";
            // line 285
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 289
        echo "                            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgWarn"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 290
            echo "                                <div class=\"span4 offset4\" >
                                    <div class=\"alert alert-block fade in alert-warning\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                        <h4>";
            // line 293
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 297
        echo "                            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgOk"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 298
            echo "                                <div class=\"span4 offset4\" >
                                    <div class=\"alert alert-block fade in alert-success\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                        <h4>";
            // line 301
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 305
        echo "                            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msgInfo"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 306
            echo "                                <div class=\"span4 offset4\" >
                                    <div class=\"alert alert-block fade in alert-info\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                                        <h4>";
            // line 309
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "</h4>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 313
        echo "                        </div>
\t\t\t\t\t\t<div class=\"preloader\">
                            <div class=\"fa fa-circle-o-notch fa-spin fa-3x fa-fw\"></div>
\t\t\t\t\t\t</div>
                        ";
        // line 317
        $this->displayBlock('body', $context, $blocks);
        // line 319
        echo "                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /#page-wrapper -->
        </div>
            <!-- /#wrapper -->
    </body>

</html>
";
    }

    // line 317
    public function block_body($context, array $blocks = array())
    {
        // line 318
        echo "                        ";
    }

    public function getTemplateName()
    {
        return "GESTIONGestionBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  549 => 318,  546 => 317,  533 => 319,  531 => 317,  525 => 313,  515 => 309,  510 => 306,  505 => 305,  495 => 301,  490 => 298,  485 => 297,  475 => 293,  470 => 290,  465 => 289,  455 => 285,  450 => 282,  446 => 281,  426 => 264,  418 => 259,  410 => 254,  397 => 244,  389 => 239,  381 => 234,  373 => 229,  365 => 224,  362 => 223,  354 => 218,  346 => 213,  343 => 212,  341 => 211,  336 => 208,  326 => 201,  318 => 196,  312 => 192,  310 => 191,  305 => 189,  295 => 182,  287 => 177,  279 => 172,  264 => 160,  258 => 157,  251 => 154,  249 => 153,  239 => 146,  235 => 145,  221 => 133,  218 => 132,  216 => 131,  208 => 128,  127 => 50,  122 => 48,  118 => 47,  113 => 45,  107 => 42,  101 => 39,  95 => 36,  87 => 33,  81 => 30,  75 => 27,  69 => 24,  65 => 23,  56 => 20,  52 => 19,  48 => 18,  44 => 17,  20 => 1,  84 => 35,  79 => 33,  72 => 29,  66 => 26,  60 => 21,  54 => 20,  39 => 15,  35 => 14,  33 => 5,  31 => 4,  28 => 2,);
    }
}
