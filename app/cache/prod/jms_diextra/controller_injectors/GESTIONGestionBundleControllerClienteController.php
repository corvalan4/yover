<?php

namespace GESTION\GestionBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class ClienteController__JMSInjector
{
    public static function inject($container) {
        $instance = new \GESTION\GestionBundle\Controller\ClienteController();
        $instance->sessionSvc = $container->get('session.manager', 1);
        return $instance;
    }
}
