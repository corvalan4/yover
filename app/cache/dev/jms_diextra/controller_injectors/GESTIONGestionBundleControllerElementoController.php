<?php

namespace GESTION\GestionBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class ElementoController__JMSInjector
{
    public static function inject($container) {
        $instance = new \GESTION\GestionBundle\Controller\ElementoController();
        $instance->sessionManager = $container->get('session.manager', 1);
        return $instance;
    }
}
